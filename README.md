# Cubix
Cubix is a 3x3x3 Rubik's cube solver. It is written in Python and works on TensorFlow and OpenCV.
I started working on this project during the 1st lockdown due to COVID-19, here in Italy, along with my daughter. 
The main objective was encouraging her to build complex things with few resources and little previous knowledge. 
And we solved our cube, eventually! :smile:

Then, during the 2nd wave, we pushed this project ahead and built a 
[solving robot](https://gitlab.com/francescoracciatti/cubot) from scratch.

# Table Of Contents
* [Usage keys](#keys)
* [Installation and run](#installation-and-run)
* [Usage](#usage)
  * [Known issues](#known-issues)
  * [How to scan your cube](#how-to-scan-your-cube)
  * [How to calibrate colors](#how-to-calibrate-colors)
  * [Solve the cube](#solve-the-cube)
* [Configuration and Customization](#configuration-and-customization)
* [Author, License and Support](#license)

# Installation and run
1. [Install the required packages](#prerequisites)
2. [Install protobuf](#protobuf-installation)
3. [Install TensorFlow Object Detection API](#tensorflow-object-detection-api-installation)

If any problem occurs, please write me or open an issue.

### 1 - Install Prerequisites
Download the project, create and activate a new venv.
```shell
$ git clone --depth 1 https://gitlab.com/francescoracciatti/cubix.git
$ cd cubix
../cubix$ python3 -m venv env
../cubix$ source ./env/bin/activate 
```

Then, install the project's [requirements](requirements.txt). 
```shell
(venv) cubix$ pip3 install -r requirements.txt
```

### 2 - Protobuf installation 
* Head to the [protoc release page](https://github.com/protocolbuffers/protobuf/releases)
* Download the latest `protoc-*-*.zip` and unzip in a venv directory `VENV_PATH_PB`
* Export the path to `protoc`:
```shell
(venv) cubix$ export PATH=$PATH:my_path_to_protoc
```

### 3 - Tensorflow Object Detection API installation
Download TensorFlow and install TensorFlow Object Detection API:
```shell
(venv) cubix$ git clone https://github.com/tensorflow/models.git
(venv) cubix$ cd models/research/
(venv) cubix/models/research$ protoc object_detection/protos/*.proto --python_out=.
(venv) cubix/models/research$ cp object_detection/packages/tf2/setup.py .
(venv) cubix/models/research$ python -m pip install .
```

Test the installation (all tests must pass):
```shell
(venv) cubix/model/research$ python object_detection/builders/model_builder_tf2_test.py
```

### Run Cubix
Export the Cubix absolute paths `abs_path_to/cubix` in `PYTHONPATH` before running.
```shell
(venv) cubix$ export PYTHONPATH=$PYTHONPATH:abs_path_to/cubix
(venv) cubix$ cd cubix/src
(venv) cubix/src$ python cubix.py
```

Make sure you run source `./env/bin/activate` every time you want to run Cubix.

# Usage

## Keybindings
* `ESC` to stop Cubix
* `SPACE` to capture the face you are scanning
* `c` to calibrate colors
* `s` to solve the current configuration
* `r` to restart Cubix from scratch
* `h` to show help

## Known issues
Please note that Cubix may not perform optimally in unfavorable lighting conditions.

## How to scan your cube
Cubix recognizes sides automatically, but the way you rotate the cube while scanning it is crucial.
* Start by with the green side facing the camera and the white on top.
* After you've scanned the green side, rotate the cube 90 or -90 degrees horizontally. 
  Direction does not matter. You can go both clockwise or counter-clockwise.
  Scan the blue, red, and orange sides until you are back on the green side.
* Now, you should be back in the starting position, with the green side facing the camera and the white on top.
* Rotate the cube 90 degrees forward to scan the white side, 
  resulting in the green side at the bottom and the white facing the camera.
* After you've scanned the white side, turn the cube back to how you started, 
  having green in front again and white on top. 
  Now rotate the cube backward 90 degrees to scan the yellow side, 
  resulting in green on top and yellow facing the camera.

Done! 

## How to calibrate colors
If color recognition does not perform well, you may try to calibrate the color palette. 
Press the key `c` to start the guided calibration procedure, and follow the instructions.
You can stop the calibration procedure any time by pressing the key `c`. 
Moreover, if you don't want to calibrate a color, press `SPACE` without framing the cube.

## Solve the cube
Once you scanned all the sides, press `s` to get the solution on screen.

Errors may occur if:
* you scanned the cube without respecting orientation;
* Cubix didn't recognize colors properly.

If so, you may try to scan the cube again.
Alternatively, you may try to calibrate colors, as described above.

# Configuration and Customization
You may find useful insights in [HowTo](HOWTO.md).

# Authors
Francesco Racciatti

# License
Cubix is licensed under the [MIT license](#LICENSE).

# Support
If you found this project useful, you may think to give me a cup of coffee.\
Thank you for your support :slightly_smiling_face:

[![](https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YS32QGERBZLUN)
