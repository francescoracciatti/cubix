"""
This modules provides the 3x3 Rubik's cube model.
"""

from __future__ import annotations

import collections
import logging
from enum import Enum, unique
from typing import Dict, List

from src.cube.visitable import Visitable
from src.cube.visitor import Visitor


@unique
class Side(Enum):
    """
    The sides in standard notation.
    """
    UP = 'U'
    LEFT = 'L'
    FRONT = 'F'
    RIGHT = 'R'
    BACK = 'B'
    DOWN = 'D'


@unique
class Color(Enum):
    """
    The standard colors + NONE (undefined color).
    """
    RED = 'red'
    ORANGE = 'orange'
    BLUE = 'blue'
    GREEN = 'green'
    WHITE = 'white'
    YELLOW = 'yellow'
    NONE = 'none'  # Undefined color

    @staticmethod
    def list() -> List[Color]:
        """
        Gets the list of standard colors (without the NONE color).

        :return: the list of standard colors
        """
        return [color for color in Color if color != Color.NONE]


class Face(object):
    """
    Models a 3x3 cube's face.
    """

    # The size of the face
    SIZE = 3

    # The number of tiles
    TILES = SIZE * SIZE

    @classmethod
    def default(cls) -> Face:
        """
        Builds a face with no colors.

        :return: a face having no colored tiles
        """
        return Face([Color.NONE for _ in range(cls.TILES)])

    def __init__(self, colors: List[Color]) -> None:
        """
        Initializes the face with the given ordered sequence of colors.

        A face is made by 3x3 colored tiles (C):

        |--------------|
        | C1 | C2 | C3 |
        |----|----|----|
        | C4 | C5 | C6 |
        |----|----|----|
        | C7 | C8 | C9 |
        |--------------|

        The initialization of a 3x3 face takes an ordered sequence of 9 colors: C1, C2, C3, C4, C5, C6, C7, C8, C9.

        :param colors: the ordered sequence of colors C1, C2, C3, C4, C5, C6, C7, C8, C9
        :raise ValueError: if the number of colors mismatches with the number of face's tiles
        """
        if len(colors) != self.TILES:
            raise ValueError(f"Got {len(colors)} colors, {self.SIZE * self.SIZE} expected")
        self.colors = colors

    @property
    def size(self) -> int:
        return self.SIZE

    @property
    def center(self) -> Color:
        """
        Gets the color of the central tile.

        :return: the color of the central tile
        """
        return self.colors[4]

    @center.setter
    def center(self, color: Color) -> None:
        """
        Sets the central tile with the given color.

        :param color: the color of the central face
        """
        self.colors[4] = color

    def flip_horizontal(self) -> Face:
        """
        Gets a copy of the face, horizontally flipped.
        Note that the source face remains unchanged.

        Horizontal flip means:
        1, 2, 3      3, 2, 1
        4, 5, 6  ->  6, 5, 4
        7, 8, 9      9, 8, 7

        :return: a copy of the face, horizontally flipped
        """
        colors = [
            self.colors[2], self.colors[1], self.colors[0],
            self.colors[5], self.colors[4], self.colors[3],
            self.colors[8], self.colors[7], self.colors[6]]
        return Face(colors)


class Cube(Visitable):
    """
    Models a 3x3 rubik cube.
    """

    # The size of the cube's faces
    SIZE = 3

    # Side to opposite (side) map
    SIDE_TO_OPPOSITE: Dict[Side, Side] = {
        # Up - Down
        Side.UP: Side.DOWN,
        Side.DOWN: Side.UP,
        # Front - Back
        Side.FRONT: Side.BACK,
        Side.BACK: Side.FRONT,
        # Left - Right
        Side.LEFT: Side.RIGHT,
        Side.RIGHT: Side.LEFT
    }

    # Standard mapping of sides
    SIDE_TO_COLOR: Dict[Side, Color] = {
        Side.UP: Color.WHITE,
        Side.LEFT: Color.RED,
        Side.FRONT: Color.BLUE,
        Side.RIGHT: Color.ORANGE,
        Side.BACK: Color.GREEN,
        Side.DOWN: Color.YELLOW
    }

    # Maps each color with its side, for convenience
    COLOR_TO_SIDE: Dict[Color, Side] = {v: k for k, v in SIDE_TO_COLOR.items()}

    def accept(self, visitor: Visitor) -> None:
        """
        Accepts the visitor.

        :param visitor: the visitor
        """
        visitor.visit_cube(self)

    def __init__(self) -> None:
        """
        Initializes a default cube with no colors, except for the central tiles.
        Central tiles get the related standard colors, depending on the side of them.
        """
        def get_standard_face(side: Side) -> Face:
            face = Face.default()
            face.center = self.SIDE_TO_COLOR[side]
            return face

        self.side_to_face: Dict[Side, Face] = {side: get_standard_face(side) for side in Side}

    def face(self, side: Side) -> Face:
        """
        Gets the face of the given side.

        :param side: the side
        :return: the face of the given side
        """
        return self.side_to_face[side]

    def update(self, side: Side, face: Face) -> None:
        """
        Updates the given face.

        :param side: the side of the face to update
        :param face: the new face
        """
        self.side_to_face[side] = face

    def is_valid(self) -> bool:
        """
        Checks if the cube's color configuration is valid.

        :return: true if the color configuration is valid, false otherwise
        """
        color_to_counter: Dict[Color, int] = collections.Counter()

        for face in self.side_to_face.values():
            # Checks colors
            if Color.NONE in face.colors:
                logging.warning(f"The face {face} contains a not valid color")
                return False

            # Maps colors with occurrences of them, and update the counter
            color_to_counter.update({color: face.colors.count(color) for color in face.colors})

        for color, counter in dict(color_to_counter).items():
            if counter != Face.TILES:
                logging.warning(f"Cube of size {self.SIZE} cannot have {counter} {color}")
                return False

        return True

    def flip_horizontal(self) -> Cube:
        """
        Gets a copy of the cube, having all the faces horizontally flipped.
        Note that the source cube remains unchanged.

        Horizontal flip means:
        1, 2, 3      3, 2, 1
        4, 5, 6  ->  6, 5, 4
        7, 8, 9      9, 8, 7

        :return: a copy of the cube, horizontally flipped
        """
        flipped = Cube()
        for side in Side:
            flipped.update(side, self.face(side).flip_horizontal())
        return flipped
