"""
This module provides the interfaces for building visitors.
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

# Digs out of the circular import due to type hint on Cube class
if TYPE_CHECKING:
    from src.cube.cube import Cube


class Visitor(ABC):
    """
    The Visitor.
    """

    @abstractmethod
    def visit_cube(self, cube: Cube) -> str:
        pass
