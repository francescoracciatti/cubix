"""
Unit test for cube.py.
"""

import random
import unittest
from abc import ABC
from typing import Dict, Optional

from src.cube.cube import Cube, Face, Side, Color


class ColorBucket(object):
    """
    Provides a bucket with available colors.
    """

    def __init__(self) -> None:
        """
        Initializes the color bucket.
        """
        self.color_to_counter: Dict[Color, int] = {color: Face.TILES for color in Color.list()}

    def pick(self, color: Color) -> Optional[Color]:
        """
        Picks the color, if available.

        :param color: the color to pick
        :return: the color if it is available, None otherwise
        """
        if self.color_to_counter[color]:
            self.color_to_counter[color] -= 1
            return color
        return None

    def pick_random(self) -> Optional[Color]:
        """
        Picks a random color, if available.

        :return: a random color if it is available, None otherwise
        """
        # Gets the first available random color
        colors = Color.list()
        random.shuffle(colors)

        for color in colors:
            if self.color_to_counter[color]:
                self.color_to_counter[color] -= 1
                return color

        return None


class CubeGenerator(ABC):
    """
    Supports the generation of cubes.
    """

    @staticmethod
    def solved() -> Cube:
        """
        Builds a solved cube.

        :return: a solved cube
        """
        # Builds a solved cube
        cube = Cube()
        for side, color in Cube.SIDE_TO_COLOR.items():
            cube.update(side, Face([color for _ in range(Face.TILES)]))
        return cube

    @staticmethod
    def random() -> Cube:
        """
        Gets a randomly initialized cube.
        Note that central tiles have the standard configuration.

        :return: the randomly configured cube
        """
        # Builds a standard cube (only central tiles are initialized)
        cube = Cube()

        # Gets the bucket, picks away central colors
        bucket = ColorBucket()
        for color in Color.list():
            bucket.pick(color)

        # Fills the cube randomly
        for side in Side:
            colors = [bucket.pick_random() for _ in range((Face.TILES - 1) // 2)]  # 1st four colours
            colors += [cube.face(side).center]
            colors += [bucket.pick_random() for _ in range((Face.TILES - 1) // 2)]  # 2nd four colours
            cube.update(side, Face(colors))

        return cube


class TestCubeGenerator(unittest.TestCase):
    """
    Test CubeGenerator utility class.
    """

    # Standard mapping of sides
    SIDE_TO_COLOR: Dict[Side, Color] = {
        Side.UP: Color.WHITE,
        Side.LEFT: Color.RED,
        Side.FRONT: Color.BLUE,
        Side.RIGHT: Color.ORANGE,
        Side.BACK: Color.GREEN,
        Side.DOWN: Color.YELLOW
    }

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_cube_solved_is_standard(self) -> None:
        """
        Checks if the colors of the central tiles of a solved cube respect the standard ordering.
        """
        cube = CubeGenerator.solved()
        for side, color in self.SIDE_TO_COLOR.items():
            self.assertEqual(self.SIDE_TO_COLOR[side], cube.face(side).center, f"Side {side}")

    def test_cube_random_is_standard(self) -> None:
        """
        Checks if the colors of the central tiles of a random cube respect the standard ordering.
        """
        cube = CubeGenerator.random()
        for side, color in self.SIDE_TO_COLOR.items():
            self.assertEqual(self.SIDE_TO_COLOR[side], cube.face(side).center, f"Side {side}")


class TestCube(unittest.TestCase):
    """
    Test cube.Cube.
    """

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_cube_is_standard(self) -> None:
        """
        Checks if the colors of the central tiles of the cube respect the standard ordering.
        """
        cube = Cube()  # Uses the cube constructor

        for side, color in TestCubeGenerator.SIDE_TO_COLOR.items():
            self.assertEqual(TestCubeGenerator.SIDE_TO_COLOR[side], cube.face(side).center, f"Side {side}")

    def test_is_valid_when_solved_configuration(self) -> None:
        """
        Tests the correctness of the function cube.is_valid when the configuration is valid.
        """
        cube = CubeGenerator.solved()
        self.assertTrue(cube.is_valid(), "The cube has a valid configuration")

    def test_is_valid_when_random_configuration(self) -> None:
        """
        Tests the correctness of the function cube.is_valid when the configuration is valid.
        """
        attempts = 10000
        for _ in range(attempts):
            cube = CubeGenerator.random()
            self.assertTrue(cube.is_valid(), "The cube has a valid configuration")

    def test_is_valid_when_color_none(self) -> None:
        """
        Tests the correctness of the function cube.is_valid when the cube contains Color.NONE.
        """
        cube = Cube()
        self.assertFalse(cube.is_valid(), "The cube contains Color.NONE facets")

    def test_is_valid_when_color_occurrences_mismatch(self) -> None:
        """
        Tests the correctness of the function cube.is_valid when the configuration is valid.
        """
        # Builds a solved cube
        cube = Cube()
        for side, color in Cube.SIDE_TO_COLOR.items():
            cube.update(side, Face([color for _ in range(Face.TILES)]))
        # Makes a face redundant
        cube.update(Side.UP, cube.face(Side.DOWN))
        self.assertFalse(cube.is_valid(), "The cube has not a valid configuration")
