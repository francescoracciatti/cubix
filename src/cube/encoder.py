"""
This module provides the encoder that builds the cube representation string.
"""

import kociemba

from src.cube.cube import Cube, Side
from src.cube.visitor import Visitor


class Kociemba(Visitor):
    """
    Builds the representation of the cube as expected by the Kociemba lib.
    """

    # Kociemba's cube description string order: URFDLB
    SIDE_ORDER = (Side.UP, Side.RIGHT, Side.FRONT, Side.DOWN, Side.LEFT, Side.BACK)

    def _get_cube_description_string(self, cube: Cube) -> str:
        """
        The representation of the 3x3 cube is the following (letters stand for Up, Left, Front, Right, Back, Down):

                        |--------------|
                        | U1 | U2 | U3 |
                        |----|----|----|
                        | U4 | U5 | U6 |
                        |----|----|----|
                        | U7 | U8 | U9 |
                        |--------------|
        |--------------||--------------||--------------||--------------|
        | L1 | L2 | L3 || F1 | F2 | F3 || R1 | R2 | R3 || B1 | B2 | B3 |
        |----|----|----||----|----|----||----|----|----||----|----|----|
        | L4 | L5 | L6 || F4 | F5 | F6 || R4 | R5 | R6 || B4 | B5 | B6 |
        |----|----|----||----|----|----||----|----|----||----|----|----|
        | L7 | L8 | L9 || F7 | F8 | F9 || R7 | R8 | R9 || B7 | B8 | B9 |
        |--------------||--------------||--------------||--------------|
                        |--------------|
                        | D1 | D2 | D3 |
                        |----|----|----|
                        | D4 | D5 | D6 |
                        |----|----|----|
                        | D7 | D8 | D9 |
                        |--------------|

        The invariant of a 3x3 cube is the center facet. Each center facet defines the color of the whole face.
        The color of each facet represents the target face that the facet has to achieve.
        So, Kociemba does not use colors directly, but represents each facet's color by using the letter related to the
        target face to which the facet belongs.

        For example, if U5 = Red and R1 = Red,
        Kociemba represents such a combination as: U5 = U and R1 = U.

        The Kociemba cube definition string follows the order URFDLB:
         U1, U2, U3, U4, U5, U6, U7, U8, U9,
         R1, R2, R3, R4, R5, R6, R7, R8, R9,
         F1, F2, F3, F4, F5, F6, F7, F8, F9,
         D1, D2, D3, D4, D5, D6, D7, D8, D9,
         L1, L2, L3, L4, L5, L6, L7, L8, L9,
         B1, B2, B3, B4, B5, B6, B7, B8, B9.

        So, for example, the a cube definition string "UBL..." means that:
         - the color of the U1 facet is U,
         - the color of the U2 facet is B,
         - the color of the U3 facet is L, and so on.

        Then, the definition string of a solved cube is:
         UUUUUUUUURRRRRRRRRFFFFFFFFFDDDDDDDDDLLLLLLLLLBBBBBBBBB

        :param cube: the cube
        :return: the cube description string
        """
        # Maps colors to the related side
        color_to_side = {face.center: side for side, face in cube.side_to_face.items()}
        # Builds the cube description string
        description = ''
        for side in self.SIDE_ORDER:
            for color in cube.face(side).colors:
                description += color_to_side[color].value.upper()
        return description

    def visit_cube(self, cube: Cube) -> str:
        """
        Builds the representation string of the cube 3x3.

        :param cube: the cube
        :return: the definition sting of the cube
        """
        return self._get_cube_description_string(cube)

    def solve(self, cube: Cube) -> str:
        """
        Solves the given cube.

        :param cube: the cube to solve
        :return: the string of moves in standard notation to solve the cube
        :raise ValueError: if the configuration of the cube is not valid
        """
        if cube.is_valid():
            description = self.visit_cube(cube)
            return kociemba.solve(description)
        else:
            return "Not a valid configuration"
