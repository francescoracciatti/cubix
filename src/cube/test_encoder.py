"""
Unit test for encoder.py.
"""

import unittest

from src.cube.cube import Side
from src.cube.encoder import Kociemba
from src.cube.test_cube import CubeGenerator


class TestKociembaEncoder(unittest.TestCase):
    """
    Tests the Kociemba encoder.
    """

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_cube_kociemba_definition_string(self) -> None:
        """
        Tests the correctness of the cube's kociemba definition string.
        """
        attempts = 10000
        try:
            for _ in range(attempts):
                cube = CubeGenerator.random()

                # Gets the color-based cube's configuration in kociemba order URFDLB
                configuration_color = []
                configuration_color += cube.face(Side.UP).colors
                configuration_color += cube.face(Side.RIGHT).colors
                configuration_color += cube.face(Side.FRONT).colors
                configuration_color += cube.face(Side.DOWN).colors
                configuration_color += cube.face(Side.LEFT).colors
                configuration_color += cube.face(Side.BACK).colors

                # Builds the conversion table from color-based to kociemba side-based
                color_to_side = {
                    cube.face(Side.UP).center: Side.UP,
                    cube.face(Side.RIGHT).center: Side.RIGHT,
                    cube.face(Side.FRONT).center: Side.FRONT,
                    cube.face(Side.DOWN).center: Side.DOWN,
                    cube.face(Side.LEFT).center: Side.LEFT,
                    cube.face(Side.BACK).center: Side.BACK,
                }

                # Gets the kociemba cube's definition string
                configuration_kociemba = ''
                for color in configuration_color:
                    configuration_kociemba += color_to_side[color].value.upper()

                # Tests the generated kociemba definition string
                self.assertEqual(configuration_kociemba, Kociemba().visit_cube(cube))

        except ValueError as e:
            self.fail(e)
