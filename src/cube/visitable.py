"""
This module provides the interfaces to make concrete classes visitable.
"""

from abc import ABC, abstractmethod

from src.cube.visitor import Visitor


class Visitable(ABC):
    """
    Makes a concrete class visitable by Visitors.
    """

    @abstractmethod
    def accept(self, visitor: Visitor) -> None:
        pass
