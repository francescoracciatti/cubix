"""
This module provides the facet detectors.
"""

import itertools
import logging
import math
from operator import itemgetter
from typing import Optional, List, Tuple, Dict

import cv2 as cv
import numpy as np

from src.detection.detection import ObjectDetection
from src.utils.utils import radians, distance, remove_outliers, degrees, performance
from src.video.palette import Palette
from src.video.render import Frame, ColorSpace, CVContour, RectBox, Circle, Point
from src.video.window import Window, WindowHandler, WindowLayer


class FacetDetector(object):
    """
    Detects the cube's face and facets.
    """

    # RBoxes approximation parameters
    _APPROX_EPSILON = 0.05
    _APPROX_ASPECT_RATIO_MIN = 0.80

    # Face expected area, with respect to the frame area
    _FACE_AREA_RATE_MIN = 0.45
    _FACE_AREA_RATE_MAX = 0.75

    # Facet expected area, with respect to the face area
    _FACET_AREA_RATE_MIN = 0.075  # With respect to the detected frame area is 0.035
    _FACET_AREA_RATE_MAX = 0.100  # With respect to the detected frame area is 0.075

    # Spacing between consecutive facets
    _SPACING_RATE_SIDE = 0.15  # The spacing between consecutive facets is at most the 15% of the facets' side

    # Max admitted error when computing facets' angles
    _RAD_MAX_ERROR = 0.05  # 0.05 rad = 2.9 deg

    # Neighbor radius
    _NEIGHBOR_RADIUS = 0.25  # The 25% of the square diagonal

    @classmethod
    def process(cls, frame_src: Frame) -> Frame:
        """
        Process the source frame to highlight edges.

        :param frame_src: the source frame to process to highlight edges
        :return: the processed frame
        """
        # Converts to HSV and gets the V channel
        _, _, raw_v = cv.split(frame_src.convert(ColorSpace.HSV).npframe)

        # Removes noise
        raw_smooth = cv.GaussianBlur(raw_v, (5, 5), 0)
        # raw_smooth = cv.bilateralFilter(raw_v, 5, 20, 20)

        # Applies OTSU threshold to calculate canny thresholds, then applies canny
        threshold_otsu, _ = cv.threshold(raw_smooth, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
        threshold_min = threshold_otsu // 2
        threshold_max = threshold_min * 2

        # raw_canny = cv.Canny(raw_otsu, threshold_min, threshold_max)
        raw_canny = cv.Canny(raw_smooth, threshold_min, threshold_max)

        # Reduces noise and dilates contours
        kernel = cv.getStructuringElement(cv.MORPH_RECT, (5, 5))
        raw_closing = cv.morphologyEx(raw_canny, cv.MORPH_CLOSE, kernel)
        raw_dilated = cv.morphologyEx(raw_closing, cv.MORPH_DILATE, kernel)

        return Frame(raw_dilated, ColorSpace.GRAY, frame_src.width, frame_src.height)

    @classmethod
    def contours(cls, frame: Frame, mode: int) -> Optional[List[CVContour]]:
        """
        Gets the contour on the given frame.

        :param frame: the frame on which detect contours
        :param mode: the contour detection mode
        :return: the list of detected contours
        """
        contours, _ = cv.findContours(frame.npframe, mode, cv.CHAIN_APPROX_SIMPLE)
        return contours

    @classmethod
    def polygons(cls, contours: List[CVContour], area_min: float, area_max: float) -> Tuple[List[RectBox],
                                                                                            List[Circle]]:
        """
        Gets polygons surrounding the facets from the given list of contours.

        :param area_min: the minimum expected area for the face's contour
        :param area_max: the maximum expected area for the face's contour
        :param contours: the contours to process
        :return: a tuple containing the detected contours,
                 the 1st position provides the well-shaped squares (high-accuracy detections),
                 the 2nd position provides other detected shapes that have a chance to be facets
        """
        # The lists of detected polygons surrounding the facets
        rboxes: List[RectBox] = []  # Well-shaped square contours
        circles: List[Circle] = []  # Circular contours

        # Step 1/2: Loops over contours to detect well-shaped squares having the expected area
        discarded: List[CVContour] = []  # Stores discarded contours for low accuracy detections
        for contour in contours:
            # Approximates the contour
            perimeter = cv.arcLength(contour, True)
            polygon = cv.approxPolyDP(contour, cls._APPROX_EPSILON * perimeter, True)

            if len(polygon) == 4:  # Looks for quads
                rbox = RectBox(cv.minAreaRect(polygon))

                # Looks for well shaped quads
                if rbox.height < rbox.width:
                    aspect_ratio = rbox.height / rbox.width
                else:
                    aspect_ratio = rbox.width / rbox.height

                if aspect_ratio >= cls._APPROX_ASPECT_RATIO_MIN and area_min <= rbox.area <= area_max:
                    # Facet contour found
                    rboxes.append(rbox)
                else:
                    discarded.append(polygon)  # Stores discarded quads
            else:
                discarded.append(polygon)  # Stores discarded shapes

        if not rboxes:
            return rboxes, circles

        # Step 2/2: Loops over discarded contours, since the central facet may be circular or octagonal
        for polygon in discarded:
            circle = Circle(cv.minEnclosingCircle(polygon))
            if area_min <= circle.area <= area_max:  # Filters by expected area
                circles.append(circle)

        return rboxes, circles

    @classmethod
    def neighbors(cls, rbox: RectBox) -> List[RectBox]:
        """
        Infers the center of the neighbors of the given facet, including itself, as if the given facet was the
        center of the face.

        :param rbox: the facet
        :return: the list of neighbors following the ordering [F1, ..., F9]
        """
        side = math.sqrt(rbox.area)
        adjusted_side = side + side * cls._SPACING_RATE_SIDE
        adjusted_diagonal = math.sqrt(adjusted_side ** 2 + adjusted_side ** 2)

        # Infers the centers of expected neighbors.
        #
        # Note that the CV angle is in the range [0, 90).
        # In a square, the CV angle is the angle between the lowest edge (the point with the highest Y coord)
        # and the next edge clockwise (the point with the lower X coordinate).
        #
        # Angle: 0 deg
        # |--------------|
        # | F3 | F6 | F9 |
        # |----|----|----|
        # | F2 | F5 | F8 |
        # |----|----|----|
        # | F1 | F4 | F7 |
        # |--------------|
        #
        # Angle: 90 deg, clockwise rotation from 0
        # |--------------|
        # | F1 | F2 | F3 |
        # |----|----|----|
        # | F4 | F5 | F6 |
        # |----|----|----|
        # | F7 | F8 | F9 |
        # |--------------|
        centers: List[Point] = [
            # F1
            Point(int(rbox.center.x + adjusted_diagonal * math.cos(radians(225 - rbox.angle))),
                  int(rbox.center.y - adjusted_diagonal * math.sin(radians(225 - rbox.angle)))),

            # F2
            Point(int(rbox.center.x + adjusted_side * math.cos(radians(180 - rbox.angle))),
                  int(rbox.center.y - adjusted_side * math.sin(radians(180 - rbox.angle)))),

            # F3
            Point(int(rbox.center.x + adjusted_diagonal * math.cos(radians(135 - rbox.angle))),
                  int(rbox.center.y - adjusted_diagonal * math.sin(radians(135 - rbox.angle)))),

            # F4
            Point(int(rbox.center.x + adjusted_side * math.cos(radians(270 - rbox.angle))),
                  int(rbox.center.y - adjusted_side * math.sin(radians(270 - rbox.angle)))),

            # F5
            rbox.center,

            # F6
            Point(int(rbox.center.x + adjusted_side * math.cos(radians(90 - rbox.angle))),
                  int(rbox.center.y - adjusted_side * math.sin(radians(90 - rbox.angle)))),

            # F7
            Point(int(rbox.center.x + adjusted_diagonal * math.cos(radians(315 - rbox.angle))),
                  int(rbox.center.y - adjusted_diagonal * math.sin(radians(315 - rbox.angle)))),

            # F8
            Point(int(rbox.center.x + adjusted_side * math.cos(radians(360 - rbox.angle))),
                  int(rbox.center.y - adjusted_side * math.sin(radians(360 - rbox.angle)))),

            # F9
            Point(int(rbox.center.x + adjusted_diagonal * math.cos(radians(45 - rbox.angle))),
                  int(rbox.center.y - adjusted_diagonal * math.sin(radians(45 - rbox.angle)))),
        ]

        return [RectBox(((center.x, center.y), (side, side), rbox.angle)) for center in centers]

    # FIXME
    @classmethod
    def angle(cls, rboxes: List[RectBox], circles: List[Circle]) -> float:
        """
        Retrieves the angle in degrees of the facets by using the centers of given detections.

        :param rboxes: the high-accuracy detections
        :param circles: the low-accuracy detections
        :return: the angle
        """
        # Needs three detections, at least
        if len(rboxes) + len(circles) <= 2:
            logging.debug(f"Cannot infer the angle, too few detections {len(rboxes)}, using average")
            return np.average([rbox.angle for rbox in rboxes])

        # Gets the permutations of three items
        centers: List[Point] = [rbox.center for rbox in rboxes] + [circle.center for circle in circles]
        triplets: List[Tuple[Point, ...]] = [triplet for triplet in itertools.permutations(centers, r=3)]

        # Gets the angle in deg for each triplet
        triplet_to_rad: Dict[Tuple[Point, ...], float] = {}
        for triplet in triplets:
            a = np.array([triplet[0].x, triplet[0].y])
            b = np.array([triplet[1].x, triplet[1].y])  # The vertex for all the permutations
            c = np.array([triplet[2].x, triplet[2].y])
            # Gets the AB BC angle
            ba = a - b
            bc = c - b
            rad = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
            triplet_to_rad[triplet] = np.abs(rad)

        # Gets the triplet having the angle on the vertex B closest to 90 deg (zero cosine)
        item: Tuple[Tuple[Point, ...], float] = min(triplet_to_rad.items(), key=itemgetter(1))
        triplet = item[0]
        rad = item[1]

        # The angle must be 90 deg, approximately
        if rad > cls._RAD_MAX_ERROR:
            logging.debug(f"Excessive angle {rad} (rad) on triplet {triplet[0]}, {triplet[1]}, {triplet[2]}, "
                          "will use the average angle of detections")
            return np.average(remove_outliers([rbox.angle for rbox in rboxes]))

        # Gets the CV angle of the facets
        p1: Point = triplet[0]
        vertex: Point = triplet[1]
        p2: Point = triplet[2]

        # The vertex is the upper point
        if vertex.y <= p1.y and vertex.y <= p2.y:
            # Gets the clockwise side from the vertex
            if p1.x > p2.x:
                target = p1
            else:
                target = p2

        # The vertex is the lower point
        elif vertex.y >= p1.y and vertex.y >= p2.y:
            # Gets the clockwise side from the vertex
            if p1.x > p2.x:
                target = p2
            else:
                target = p1

        # The vertex is the point furthest to the left, between the two other points on the y axis
        elif vertex.x <= p1.x and vertex.x <= p2.x:
            # Gets the counter-clockwise side from the vertex
            if p1.y > p2.y:
                target = p1
            else:
                target = p2

        # The vertex is the point furthest to the right, between the two other points on the y axis
        elif vertex.x >= p1.x and vertex.x >= p2.x:
            # Gets the counter-clockwise side from the vertex
            if p1.y > p2.y:
                target = p1
            else:
                target = p2

        # The vertex is between the two other points on both axes
        # P1, Vertex, and P2 make a slight obtuse angle
        # The cube has a slight angle with respect to the horizontal plane
        # Uses the delta y to choose the target side
        elif np.abs(vertex.y - p1.y) < np.abs(vertex.y - p2.y):
            target = p1

        elif np.abs(vertex.y - p1.y) > np.abs(vertex.y - p2.y):
            target = p2

        else:
            logging.warning(f"Cannot infer angle on triplet {p1}, {vertex}, {p2}, "
                            "will use the average angle of detections")
            return np.average(remove_outliers([rbox.angle for rbox in rboxes]))

        deg = degrees(np.arccos(np.abs(target.x - vertex.x) / distance(target.x, target.y, vertex.x, vertex.y)))
        return deg

    @classmethod
    def facets(cls, rboxes: List[RectBox], circles: List[Circle]) -> List[Optional[RectBox]]:
        """
        Selects the best 9 rboxes from the given list of detections.

        :param rboxes: the list of rboxes (high accuracy detections)
        :param circles: the list of circles (low accuracy detections)
        :return: the list of the 9 detected facets following the ordering [F1, ..., F9]
        """
        if not rboxes:  # Cannot reshape low accuracy detections
            return [None for _ in range(9)]

        # Step 1/3: reshapes the polygons by using the average side and angle of high-accuracy detections
        # deg: float = cls.angle(rboxes, circles)
        deg: float = np.average(remove_outliers([rbox.angle for rbox in rboxes]))
        side: float = np.sqrt(np.average(remove_outliers([rbox.area for rbox in rboxes])))

        reshaped: [RectBox] = []

        # Reshapes rboxes, applies the average angle and side
        for rbox in rboxes:
            cvrect = ((rbox.center.x, rbox.center.y), (side, side), deg)
            reshaped.append(RectBox(cvrect))

        # Reshapes circles as rboxes, applies the average angle and side
        for circle in circles:
            cvrect = ((circle.center.x, circle.center.y), (side, side), deg)
            reshaped.append(RectBox(cvrect))

        # Step 2/3: for each reshaped rbox, calculates the map of its expected neighbors,
        #           as it was the central facet of the cube

        # Spacing between facets
        rbox_to_neighbors: Dict[RectBox, List[Optional[RectBox]]] = {}
        for rbox in reshaped:
            # Infers the centers of expected neighbors
            centers: List[Point] = [neighbor.center for neighbor in cls.neighbors(rbox)]

            # Gets the neighbors for the current rbox
            rbox_to_neighbors[rbox] = []
            for index, center in enumerate(centers):
                # Sorts by distance (lower first)
                neighbors = [r for r in reshaped]
                neighbors.sort(key=lambda n: distance(center.x, center.y, n.center.x, n.center.y), reverse=False)
                # Gets the nearest, if exists
                if distance(center.x, center.y,
                            neighbors[0].center.x, neighbors[0].center.y) < side * cls._NEIGHBOR_RADIUS:
                    rbox_to_neighbors[rbox].append(neighbors[0])
                    logging.debug(f"{rbox}' neighbor F{index + 1}: {neighbors[0]}")
                else:
                    rbox_to_neighbors[rbox].append(None)  # Ensures the ordering [F1, ..., F9]
                    logging.debug(f"{rbox}' neighbor F{index + 1}: None")

        if not rbox_to_neighbors:
            logging.critical(f"Should never happen")
            return [None for _ in range(9)]

        for k, v in rbox_to_neighbors.items():
            if v.count(None) == 9:
                logging.critical(f"Should never happen, any face is neighbor of itself")
                return [None for _ in range(9)]

        # Step 3/3: gets the configuration having the highest number of neighbors
        #           and the centroid closest to the centroid of all detections

        centroid_detection = Point(np.average([rbox.center.x for rbox in reshaped]),
                                   np.average([rbox.center.y for rbox in reshaped]))

        facets: List[RectBox] = list(filter(lambda r: rbox_to_neighbors[r].count(None) == 0,
                                            [r for r in reshaped]))

        if not facets:  # Some facets were undetected
            # Retrieves the facets with the highest number of neighbors (lower number of None)
            facets = [rbox for rbox in reshaped]
            facets.sort(key=lambda f: rbox_to_neighbors[f].count(None), reverse=False)

            eligibles = []
            for facet in facets:
                if rbox_to_neighbors[facet].count(None) == rbox_to_neighbors[facets[0]].count(None):
                    eligibles.append(facet)
                    logging.debug(f"Add eligible facet {facet}")

            # For such facets, gets the ones with the best centroid
            facet_to_distance: Optional[Tuple[RectBox, float]] = None
            for facet in eligibles:
                neighbors = []
                for n in rbox_to_neighbors[facet]:
                    if n:
                        neighbors.append(n)

                centroid = Point(np.average([n.center.x for n in neighbors]),
                                 np.average([n.center.y for n in neighbors]))
                dist = distance(centroid.x, centroid.y, centroid_detection.x, centroid_detection.y)
                if not facet_to_distance or dist < facet_to_distance[1]:
                    facet_to_distance = (facet, dist)

            return rbox_to_neighbors[facet_to_distance[0]]

        else:  # All facets were detected
            # Gets the configuration having the best centroid
            facets.sort(key=lambda f: distance(centroid_detection.x, centroid_detection.y, f.center.x, f.center.y),
                        reverse=False)
            return rbox_to_neighbors[facets[0]]

    @classmethod
    def infer(cls, rboxes: List[Optional[RectBox]]) -> List[Optional[RectBox]]:
        """
        Completes the given list of detected facets by inferring the position of undetected ones, if possible.

        Missing facets can be inferred if the list of the detected facets contains the following configurations:
         - corner: F1, F3, F7, F9
         - cross: F2, F4, F6, F8

          |--------------|
          | F1 | F2 | F3 |
          |----|----|----|
          | F4 | F5 | F6 |
          |----|----|----|
          | F7 | F8 | F9 |
          |--------------|

        :param rboxes: the list of detected facets
        :return: the list of detected facets + inferred facets
        """
        # Configurations to infer missing detections
        corners: List[RectBox] = [rboxes[0], rboxes[2], rboxes[6], rboxes[8]]  # F1, F3, F7, F9
        crosses: List[RectBox] = [rboxes[1], rboxes[3], rboxes[5], rboxes[7]]  # F2, F4, F6, F8
        # y1 = [rboxes[0], rboxes[2], rboxes[7]]  # F1, F3, F8
        # y2 = [rboxes[1], rboxes[6], rboxes[8]]  # F2, F7, F9
        # y3 = [rboxes[0], rboxes[5], rboxes[6]]  # F1, F6, F7
        # y4 = [rboxes[2], rboxes[3], rboxes[8]]  # F3, F4, F9
        # l1 = [rboxes[0], rboxes[2], rboxes[6]]  # F1, F3, F7
        # l2 = [rboxes[0], rboxes[2], rboxes[8]]  # F1, F3, F9
        # l3 = [rboxes[0], rboxes[6], rboxes[8]]  # F1, F7, F9
        # l4 = [rboxes[2], rboxes[6], rboxes[8]]  # F3, F7, F9

        # Gets mean angle and side
        detections: List[RectBox] = list(filter(lambda rbox: rbox is not None, [rbox for rbox in rboxes]))
        angle: float = np.average([detection.angle for detection in detections])
        side: float = math.sqrt(np.average([detection.area for detection in detections]))

        # Checks if the missing positions can be inferred and gets the central tile
        if None not in corners:
            center = Point(np.average([rbox.center.x for rbox in corners]),
                           np.average([rbox.center.y for rbox in corners]))
        elif None not in crosses:
            center = Point(np.average([rbox.center.x for rbox in crosses]),
                           np.average([rbox.center.y for rbox in crosses]))
        # TODO Handle Y configurations
        # TODO Handle L configurations
        else:  # Cannot infer
            logging.debug(f"Cannot infer from {rboxes}")
            return rboxes

        # Infers the central facet and its neighbors
        if not rboxes[4]:
            cvrect = ((center.x, center.y), (side, side), angle)
            rboxes[4] = RectBox(cvrect)
        neighbors: List[RectBox] = cls.neighbors(rboxes[4])

        # Infers undetected facets
        for index, rbox in enumerate(rboxes):
            if not rbox:
                rboxes[index] = neighbors[index]

        return rboxes

    @classmethod
    @performance
    def detect(cls, frame_src: Frame) -> ObjectDetection:
        """
        Detects the facets in the given frame, ordered from top-left to bottom-right:

        |--------------|
        | F1 | F2 | F3 |
        |----|----|----|
        | F4 | F5 | F6 |
        |----|----|----|
        | F7 | F8 | F9 |
        |--------------|

        F1 = top-left
        F5 = center
        F9 = bottom-right

        :return: a tuple containing the processed frame and the ordered list of 9 facets [F1, ..., F9]
        """
        # Processes the frame to highlight the edges
        frame_processed = cls.process(frame_src)

        WindowHandler.register(Window(frame_processed, "1 - Processed", WindowLayer.DEBUG)
                               .move(Point(1125, 0)).resize((200, 200)))

        # Detects the contour of the facets
        contours = cls.contours(frame_processed, cv.RETR_CCOMP)
        frame_contours = frame_src.draw(contours, color=Palette.CSS_BGR_GREEN)
        WindowHandler.register(Window(frame_contours, "2 - Contours", WindowLayer.DEBUG)
                               .move(Point(1350, 0))
                               .resize((200, 200)))

        # Gets polygons surrounding facets from the detected contours, rboxes have higher accuracy than circles
        area_min = frame_processed.area * cls._FACE_AREA_RATE_MIN * cls._FACET_AREA_RATE_MIN
        area_max = frame_processed.area * cls._FACE_AREA_RATE_MAX * cls._FACET_AREA_RATE_MAX
        rboxes, circles = cls.polygons(contours, area_min, area_max)
        frame_detections = frame_src.draw(rboxes, color=Palette.CSS_BGR_GREEN).draw(circles, color=Palette.CSS_BGR_RED)
        WindowHandler.register(Window(frame_detections, "3 - Detections", WindowLayer.DEBUG)
                               .move(Point(1125, 250))
                               .resize((200, 200)))

        # Gets the contour of facets from detected polygons
        facets: List[Optional[RectBox]] = cls.facets(rboxes, circles)
        frame_facets = frame_src.draw(facets, color=Palette.CSS_BGR_GREEN)
        WindowHandler.register(Window(frame_facets, "4 - Facets", WindowLayer.DEBUG)
                               .move(Point(1350, 250))
                               .resize((200, 200)))

        # Infers undetected facets
        inferred: List[Optional[RectBox]] = cls.infer(facets)
        frame_inferred = frame_src.draw(inferred, color=Palette.CSS_BGR_GREEN)
        WindowHandler.register(Window(frame_inferred, "5 - Inferred", WindowLayer.DEBUG)
                               .move(Point(1125, 500))
                               .resize((200, 200)))

        return ObjectDetection(frame_src, frame_src.draw(inferred), facets)
