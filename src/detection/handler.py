"""
This module provides the detection handler.
"""
import logging
from typing import Tuple, Optional, List

import numpy as np

from src.cube.cube import Color
from src.detection.color import ColorDetector
from src.detection.cube import CubeDetector
from src.detection.detection import ObjectDetection, ColorDetection
from src.detection.facet import FacetDetector
from src.utils.utils import performance
from src.video.palette import Palette, ColorBGR
from src.video.render import Frame, Circle


class DetectionHandler(object):
    """
    Handles the detection process and provides detection results.
    """

    def __init__(self, cube_detector: CubeDetector) -> None:
        """
        Initializes the detection handler.

        :param cube_detector: the cube detector
        """
        self.cube_detector = cube_detector

    @performance
    def detect_central_color(self, frame_src: Frame) -> Tuple[Frame, Optional[ColorBGR]]:
        """
        Detects the color of the central tile on the given frame.

        The result of the detection process are:
         i) the output frame, i.e. a copy of the source frame on which the detector drew detection boxes (if any);
         ii) the BGR encoding of the color of the central facet (if any).

        :param frame_src: the source frame
        :return: a tuple made up by i) the output frame with detection, if any; and ii) the BGR encoding of the color
                 of the central facet (if any)
        """
        # Step 1/3: Detects the cube
        detection_cube = self.cube_detector.detect(frame_src)
        frame_out = detection_cube.frame_out  # Draws the cube's bounding box, if the cube was detected

        if not detection_cube.detected:  # No detection
            return frame_out, None

        # Step 2/3: Detects the facets
        detection_facets: ObjectDetection = FacetDetector.detect(detection_cube.roi(0))

        # Converts detections to circles, gets rid of the angle
        facets: List[Optional[Circle]] = []
        for rbox in detection_facets.rboxes:
            if rbox:
                facets.append(Circle(((rbox.center.x, rbox.center.y), np.sqrt(rbox.area) / 2)))
            else:
                facets.append(None)
        offset = detection_cube.rboxes[0].offset  # Facets were detected on the ROI enclosing the cube

        # Tracks the central facet on the output frame
        if not facets[4]:
            return frame_out, None
        frame_out = frame_out.draw([facets[4]], offset=offset)  # draw method expects a list of objects

        # Step 3/3: Crops the facets' ROIs and detects the colors
        try:
            # Reduces roi of 50%
            rbox = detection_facets.rboxes[4].resize(0.5)
            dominant = ColorDetector.dominant(detection_cube.rois[0].crop(rbox))
            dominant_bgr = tuple(int(d) for d in dominant)
            return frame_out, dominant_bgr
        except Exception as e:
            logging.warning(f"Exception caught: {e}")
            return frame_out, None

    @performance
    def detect_colors(self, frame_src: Frame, palette: Palette) -> Tuple[Frame, ColorDetection]:
        """
        Detects the colors of the given face

        The result of the detection process are:
         i) the output frame, i.e. a copy of the source frame on which the detector drew detection boxes (if any);
         ii) the list of 9 detected colors (Gray for undetected colors).

        :param frame_src: the source frame
        :param palette: the color palette
        :return: a tuple made up by i) the output frame with detection, if any; and ii) the list of detected colors
        """
        # Step 1/3: Detects the cube
        detection_cube = self.cube_detector.detect(frame_src)
        frame_out = detection_cube.frame_out  # Draws the cube's bounding box, if the cube was detected

        if not detection_cube.detected:  # No detection
            return frame_out, ColorDetection.default(Color.NONE)

        # Step 2/3: Detects the facets
        detection_facets: ObjectDetection = FacetDetector.detect(detection_cube.roi(0))

        # Converts detections to circles, gets rid of the angle
        facets: List[Optional[Circle]] = []
        for rbox in detection_facets.rboxes:
            if rbox:
                facets.append(Circle(((rbox.center.x, rbox.center.y), np.sqrt(rbox.area) / 2)))
            else:
                facets.append(None)
        offset = detection_cube.rboxes[0].offset  # Facets were detected on the ROI enclosing the cube

        # Draws facets' detections on the output frame
        frame_out = frame_out.draw(facets, offset=offset)

        colors: List[Color] = []
        if not detection_facets.rois:
            return frame_out, ColorDetection.default(Color.NONE)

        # Step 3/3: Crops the facets' ROIs and detects the colors
        for roi in detection_facets.rois:
            if not roi:
                colors.append(Color.NONE)
            else:
                colors.append(ColorDetector.detect(roi, palette))

        return frame_out, ColorDetection(colors)
