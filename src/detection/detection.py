"""
This module provides wrapper for detections.
"""

from __future__ import annotations

from typing import List, Optional

from src.cube.cube import Color
from src.video.render import Frame, RectBox, Offset


class ObjectDetection(object):
    """
    Models the result of a generic detection process of homologous targets (since rboxes are unlabeled).
    """

    def __init__(self, frame_src: Frame, frame_out: Frame, rboxes: Optional[List[Optional[RectBox]]]) -> None:
        """
        Initializes the detection.

        :param frame_src: the source frame on which the detection process ran
        :param frame_out: the output frame produced by the detection process
        :param rboxes: the optional list of (unlabeled) detected ROIs, None/empty means "no detection"
        """
        self.frame_src: Frame = frame_src
        self.frame_out: Frame = frame_out
        self.rboxes: Optional[List[Optional[RectBox]]] = rboxes

    @property
    def detected(self) -> bool:
        """
        Gets True if at least one target was detected, False otherwise.

        :return: True if at least one target was detected, False otherwise
        """
        if not self.rboxes:
            return False
        if not len(self.rboxes):
            return False
        if all(rbox is None for rbox in self.rboxes):
            return False
        return True

    @property
    def detections(self) -> int:
        """
        Gets the number of detected targets.

        :return: the number of detected targets
        """
        if not self.detected:
            return 0
        return sum(rbox is not None for rbox in self.rboxes)

    @property
    def rois(self) -> Optional[List[Optional[Frame]]]:
        """
        Gets all the detected ROIs, cropped from the source frame.

        :return: the ROIs as frames
        """
        if not self.detected:
            return None

        rois: List[Optional[Frame]] = []
        for rbox in self.rboxes:
            if rbox is not None:
                rois.append(self.frame_src.crop(rbox))
            else:
                rois.append(None)
        return rois

    def roi(self, index: int) -> Optional[Frame]:
        """
        Gets the ROI related to the given index.
        The ROI is cropped from the source frame.

        :param index: the index
        :return: the ROI if exists, None otherwise
        """
        if not self.rboxes or index >= len(self.rboxes) or self.rboxes[index] is None:
            return None
        return self.frame_src.crop(self.rboxes[index])

    def offset(self, index: int) -> Optional[Offset]:
        """
        Gets the offset of the rbox related to the given index.
        The offset is the top-left corner of the rbox with respect to the top-left corner of the source frame.

        :param index: the index
        :return: the offset of the rbox if exists, None otherwise
        """
        if not self.rboxes or index >= len(self.rboxes) or self.rboxes[index] is None:
            return None
        top_left = self.rboxes[index].top_left
        return Offset(top_left.x, top_left.y)


class ColorDetection(object):
    """
    Models the result of color detection.
    It wraps a list of colors.
    """

    @classmethod
    def default(cls, color: Color) -> ColorDetection:
        """
        Builds a default detection with the given color.

        :param color: the color
        :return: the default colors detection
        """
        colors = [color for _ in range(9)]
        return ColorDetection(colors)

    def __init__(self, colors: List[Color]) -> None:
        """
        Initializes the detected configuration:

        |--------------|
        | C1 | C2 | C3 |
        |----|----|----|
        | C4 | C5 | C6 |
        |----|----|----|
        | C7 | C8 | C9 |
        |--------------|

        Undetected tiles get GRAY as color.

        :param colors: the ordered sequence of colors C1, C2, C3, C4, C5, C6, C7, C8, C9
        """
        self.colors = colors

    @property
    def center(self) -> Color:
        return self.colors[4]
