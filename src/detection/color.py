"""
This module provides the color detector.
"""

import logging

import cv2 as cv
import numpy as np

from src.cube.cube import Color
from src.utils.utils import CIEDE2000
from src.video.palette import Palette
from src.video.render import Frame


class ColorDetector(object):
    """
    Detects the dominant color in the given ROI.
    """

    @classmethod
    def dominant(cls, frame_src: Frame):
        """
        Gets the dominant color of the given frame through color quantization.

        :param frame_src: the source frame
        :return: the dominant color
        :raise: Exception (unspecified error) if frame.rows < number of clusters
        """
        # The input frame has three features, i.e. the three main channels, namely B, G, R.
        # Reshapes the input frame to a float32 array of Mx3 size (M is the number of pixels)
        raw = np.float32(frame_src.npframe.reshape(-1, 3))

        # Quantization, terminates when reaching the maximum number of elements or the desired accuracy
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 200, .1)
        clusters = 1  # The number of clusters
        _, labels, palette = cv.kmeans(raw, clusters, None, criteria, 10, cv.KMEANS_RANDOM_CENTERS)
        _, counts = np.unique(labels, return_counts=True)
        dominants = tuple(palette[np.argmax(counts)])
        return dominants

    @classmethod
    def detect(cls, frame_src: Frame, palette: Palette) -> Color:
        """
        Detects the BGR color of the given ROI.
        If the color cannot be detected, it returns GRAY.

        :param frame_src: the source frame
        :param palette: the color palette
        :return: the BGR color
        """
        # Gets the dominant BGR color
        try:
            dominant_bgr = cls.dominant(frame_src)
        except Exception as e:
            logging.warning(f"Exception caught: {e}")
            return Color.NONE

        # Converts to CIE-Lab
        dominant_lab = CIEDE2000.bgr_to_lab(dominant_bgr)

        # Loops over the palette to find the nearest color (uses RED as starting point)
        distance_nearest: float = CIEDE2000.distance(dominant_lab, CIEDE2000.bgr_to_lab(palette.red))
        color_nearest: Color = Color.RED
        for color in Color.list():
            distance = CIEDE2000.distance(dominant_lab, CIEDE2000.bgr_to_lab(palette.encoding(color)))
            if distance < distance_nearest:
                distance_nearest = distance
                color_nearest = color

        return color_nearest
