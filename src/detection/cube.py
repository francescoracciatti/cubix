"""
This module provides the cube detectors.
"""

import configparser
import logging
import os
from abc import ABC, abstractmethod

import numpy as np
import tensorflow as tf
from PIL import Image
from object_detection.utils import label_map_util, visualization_utils

from src.detection.detection import ObjectDetection
from src.utils.utils import performance
from src.video.render import Frame, RectBox, ColorSpace


class CubeDetector(ABC):
    """
    Abstract Base Class to build Cube Detectors.
    """

    @abstractmethod
    def load(self) -> None:
        pass

    @abstractmethod
    def detect(self, frame: Frame) -> ObjectDetection:
        pass


class TensorFlowCubeDetector(CubeDetector):
    """
    Models a TensorFlow based Cube Detector.
    """

    # Minimum threshold for positive detections
    _MIN_SCORE_THRESHOLD = 0.85

    def __init__(self, path_to_saved_model: str, path_to_label_map: str) -> None:
        """
        Initializes the cube detector.

        :param path_to_saved_model: the path to the TensorFlow saved_model
        :param path_to_label_map: the path to the label_map.pbtxt
        """
        self._path_to_saved_model = path_to_saved_model
        self._path_to_label_map = path_to_label_map
        self._detect_fn = None
        self._category_index = None

    @performance
    def load(self) -> None:
        """
        Loads the TensorFlow saved_model and its category index.
        """
        # Loads the saved_model
        logging.info(f"{self.__class__.__name__} loading saved_model from {self._path_to_saved_model}")
        self._detect_fn = tf.saved_model.load(self._path_to_saved_model)
        logging.info(f"saved_model loaded")

        # Loads category index
        logging.info(f"{self.__class__.__name__} loading category index from {self._path_to_label_map}")
        self._category_index = label_map_util.create_category_index_from_labelmap(self._path_to_label_map)
        logging.info(f"Category index loaded")

        # Turns-off TensorFlow debug traces
        logging.info(f"{self.__class__.__name__} PIL logger level set to INFO (turns-off TensorFlow debug traces)")
        logging.getLogger('PIL').setLevel(logging.INFO)

    @performance
    def detect(self, frame_src: Frame) -> ObjectDetection:
        """
        Detects the cube in the given frame.

        :param frame_src: the source frame
        :return: the detection result
        """
        frame_rgb = frame_src.convert(ColorSpace.RGB)

        # Represents the string as an array
        image_np = np.array(Image.fromarray(frame_rgb.npframe))

        # Expands dimensions since the model expects images to have shape: [1, None, None, 3]
        tensor = tf.convert_to_tensor(image_np)
        tensor = tensor[tf.newaxis, ...]

        # Detects the cube
        detections = self._detect_fn(tensor)

        num_detections = int(detections.pop('num_detections'))
        detections = {key: value[0, :num_detections].numpy() for key, value in detections.items()}
        detections['num_detections'] = num_detections

        # detection_classes should be ints
        detections['detection_classes'] = detections['detection_classes'].astype(np.int64)

        # Draws the detection boxes and labels
        image_np_with_detections = image_np.copy()
        visualization_utils.visualize_boxes_and_labels_on_image_array(
            image_np_with_detections,
            detections['detection_boxes'],
            detections['detection_classes'],
            detections['detection_scores'],
            self._category_index,
            use_normalized_coordinates=True,
            max_boxes_to_draw=1,
            min_score_thresh=self._MIN_SCORE_THRESHOLD,
            agnostic_mode=False)

        # Returns the detection
        if detections['detection_scores'][0] >= self._MIN_SCORE_THRESHOLD:
            # TF returns an RGB frame
            frame_detection = Frame(image_np_with_detections, ColorSpace.RGB, frame_src.width, frame_src.height)
            rbox = RectBox.build_from_tf_bounding_box(frame_src, detections['detection_boxes'][0])
            return ObjectDetection(frame_src, frame_detection.convert(ColorSpace.BGR), [rbox])

        return ObjectDetection(frame_src, frame_src, None)  # No detection


class CubeDetectorCreator(ABC):
    """
    Provides the factory method to create cube detectors.
    """

    @abstractmethod
    def instantiate(self) -> CubeDetector:
        pass


class TensorFlowCubeDetectorCreator(CubeDetectorCreator):
    """
    Concrete creator for building TensorFlow based cube detectors.
    """

    def __init__(self, path_to_saved_model: str, path_to_label_map: str) -> None:
        """
        Initializes the cube detector.

        :param path_to_saved_model: the path to the TensorFlow saved_model
        :param path_to_label_map: the path to the label_map.pbtxt
        """
        self._path_to_saved_model = path_to_saved_model
        self._path_to_label_map = path_to_label_map

    def instantiate(self) -> CubeDetector:
        """
        Creates the concrete TensorFlow based cube detector.
        """
        return TensorFlowCubeDetector(self._path_to_saved_model, self._path_to_label_map)


class CubeDetectorConfigurator(ABC):
    """
    Picks a certain cube detector depending on the current configuration.
    """

    @classmethod
    def detector(cls, path_to_configuration_file: str) -> CubeDetector:
        """
        Gets an instance of a cube detector basing on the current configuration.

        :param path_to_configuration_file: the path to the configuration file
        :raise: ValueError if the detection platform in the configuration file is not supported
        """
        # Reads the configuration file
        configuration = configparser.ConfigParser()
        configuration.read(path_to_configuration_file)

        # Gets the detection platform
        platform = configuration['Detection']['Platform']
        if platform.lower() == 'tensorflow':
            path_to_saved_model = os.path.abspath(configuration['Detection.TensorFlow']['PathToSavedModel'])
            path_to_label_map = os.path.abspath(configuration['Detection.TensorFlow']['PathToLabelMap'])
            creator = TensorFlowCubeDetectorCreator(path_to_saved_model, path_to_label_map)
        else:
            err_msg = f"{cls.__class__.__name__} platform {platform} not supported"
            raise ValueError(err_msg)

        return creator.instantiate()
