#!/usr/bin/env python3

"""
Cubix is an ML based tool to solve the Rubik's cubes.
"""

import logging
import os

from src.detection.cube import CubeDetectorConfigurator
from src.utils.utils import get_logging_level
from src.video.video import Video

# The path to the configuration file
PATH_TO_CONFIGURATION_FILE = 'configuration.ini'

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=get_logging_level(PATH_TO_CONFIGURATION_FILE))


if __name__ == '__main__':
    """
    Cubix entry point.
    """
    logging.info("Hi, I'm Cubix, I'm running!")

    # Loads the cube detector
    logging.info("Loading cube detector")
    detector = CubeDetectorConfigurator.detector(os.path.abspath(PATH_TO_CONFIGURATION_FILE))
    detector.load()

    # Initializes the video handler
    logging.info("Initializing video device")
    webcam_index = 0
    Video.configure(os.path.abspath(PATH_TO_CONFIGURATION_FILE))
    video = Video(webcam_index, detector)

    # Runs the video stream and detect the cube
    logging.info("Video streaming ...")
    video.run()

    logging.info("See you later!")
