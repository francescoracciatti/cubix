"""
This module provides the supported commands.
"""

import logging
from abc import ABC
from collections import namedtuple
from enum import Enum, unique
from typing import List

import cv2 as cv

_Command = namedtuple('_Command', ['keymap', 'display_string'])


@unique
class Command(Enum):
    """
    Supported commands.
    Maps the command to the related key.
    """
    NONE = _Command(255, "No command")
    EXIT = _Command(27, "[ESC] exit")
    HELP = _Command(104, "[h] toggle help")
    RESET = _Command(114, "[r] reset the cube's configuration")
    SOLVE = _Command(115,  "[s] solve the cube")
    CAPTURE = _Command(32, "[SPACE] scan the current target")
    CALIBRATE = _Command(99, "[c] calibrate colors")

    @property
    def keymap(self) -> int:
        return self.value.keymap

    @property
    def display_string(self) -> str:
        return self.value.display_string

    @staticmethod
    def help() -> List[str]:
        """
        Gets the ordered list of commands.

        :return: a list of str containing the commands
        """
        _help: List[str] = []
        for c in Command:
            if c != Command.NONE:
                _help.append(c.display_string)

        return _help


class Keyboard(ABC):
    """
    Catches inputs from the keyboard.
    """

    # Max waiting time for input
    _WAIT_DELAY_MS = 1

    # Maps keys to related commands for convenience
    _KEY_TO_COMMAND = {c.keymap: c for c in Command}

    @classmethod
    def catch(cls) -> Command:
        """
        Catches commands from the keyboard.

        :return: the input command, if any
        """
        # Wait for an input key for at most 1 ms
        k = cv.waitKey(cls._WAIT_DELAY_MS) & 0xFF

        # Recognize the input key, if any
        command: Command = cls._KEY_TO_COMMAND.get(k)

        # Command not recognized
        if command is None:
            logging.info(f"Got key '{k}', command not recognized")
            return Command.NONE

        # Avoids trace flooding
        if command != Command.NONE:
            logging.info(f"Got key '{k}', command '{command}'")

        return command
