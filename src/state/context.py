"""
This module provides the context, the states of the system and the state machine.
"""

from __future__ import annotations

import abc
import logging
from abc import ABC, abstractmethod
from typing import Optional

from autologging import traced

from src.cube.cube import Cube, Face, Color
from src.cube.encoder import Kociemba
from src.cube.test_cube import CubeGenerator
from src.detection.cube import CubeDetector
from src.detection.handler import DetectionHandler
from src.state.command import Command
from src.video.gui import DrawerCube, DrawerFaceCapture, DrawerPalette, DrawerText, DrawerColorCapture
from src.video.palette import Palette, ColorSpace, ColorBGR
from src.video.render import Frame


class Context:
    """
    Represents the current context of the system.
    The context delegates part of its behavior to the current State object.
    Each state acts on the GUI.
    """

    def __init__(self, state: State, detector: CubeDetector) -> None:
        """
        Initializes the context.

        :param state: the initial state
        :param detector: the cube detector
        """
        # The current state of the context
        self._state = None

        # The detection handler to detect colors
        self.detection_handler = DetectionHandler(detector)

        # Stores the last scanned face (face Capture)
        self.face = Face.default()

        # Stores the last scanned color (color Calibration)
        self.color: Optional[ColorBGR] = None

        # Stores the configuration of the cube
        self.cube = Cube()

        # Stores the solving string
        self.solution = ''

        # The color palette
        self.palette = Palette(ColorSpace.BGR)

        # Stores the GUI drew by states
        self.gui = None

        # Goes to the given state
        self.transition_to(state)

    def reset(self) -> None:
        """
        Resets the cube and the palette.
        """
        self.cube = Cube()
        self.palette = Palette(ColorSpace.BGR)

    @property
    def state(self) -> State:
        return self._state

    @state.setter
    def state(self, state: State) -> None:
        self._state = state

    def transition_to(self, next_state: State) -> None:
        """
        Allows to change the state at runtime.

        :param next_state: the next state
        """
        print(f"Transition to {next_state.__class__.__name__}")
        self._state = next_state
        self._state.context = self

    def request_run(self, frame: Frame) -> None:
        """
        Runs the current state.

        :param frame: the input frame
        """
        self._state.run(frame)

    def request_help(self, frame: Frame) -> None:
        """
        Executes the help command in the current context.

        :param frame: the input frame
        """
        self._state.help(frame)

    def request_reset(self) -> None:
        """
        Executes a reset command in the current context.
        """
        self._state.reset()

    def request_solve(self, frame: Frame) -> None:
        """
        Executes a solve command in the current context.

        :param frame: the input frame
        """
        self._state.solve(frame)

    def request_capture(self) -> None:
        """
        Executes a capture command in the current context.
        """
        self._state.capture()

    def request_calibrate(self, frame: Frame) -> None:
        """
        Executes a calibrate command in the current context.

        :param frame: the input frame
        """
        self._state.calibrate(frame)


@traced
class State(ABC):
    """
    The base class to implement states.
    The State back-references the Context through properties and methods.
    This back-reference can be used by States to transition the Context to another State.
    """

    def __init__(self) -> None:
        """
        Initializes the state.
        """
        self._context = None

    @property
    def context(self) -> Context:
        return self._context

    @context.setter
    def context(self, context: Context) -> None:
        self._context = context

    @property
    def gui(self) -> Optional[Frame]:
        return self.context.gui

    @gui.setter
    def gui(self, frame: Frame) -> None:
        self.context.gui = frame

    @property
    def face(self) -> Face:
        return self.context.face

    @face.setter
    def face(self, face: Face) -> None:
        self.context.face = face

    @property
    def color(self) -> ColorBGR:
        return self.context.color

    @color.setter
    def color(self, color: ColorBGR) -> None:
        self.context.color = color

    @property
    def cube(self) -> Cube:
        return self.context.cube

    @cube.setter
    def cube(self, cube: Cube) -> None:
        self.context.cube = cube

    @property
    def solution(self) -> str:
        return self.context.solution

    @solution.setter
    def solution(self, text: str) -> None:
        self.context.solution = text

    @property
    def palette(self) -> Palette:
        return self.context.palette

    @property
    def detection_handler(self) -> DetectionHandler:
        return self.context.detection_handler

    def transition_to(self, next_state: State) -> None:
        self.context.transition_to(next_state)

    @abstractmethod
    def run(self, frame: Frame) -> None:
        """
        Runs the current state.
        """
        pass

    @abstractmethod
    def help(self, frame: Frame) -> None:
        """
        Toggles the complete list of commands.
        """
        pass

    @abstractmethod
    def reset(self) -> None:
        """
        Restarts from scratch.
        """
        pass

    @abstractmethod
    def solve(self, frame: Frame) -> None:
        """
        Solves the current configuration, if possible.
        """
        pass

    @abstractmethod
    def capture(self) -> None:
        """
        Captures the current target.
        """
        pass

    @abstractmethod
    def calibrate(self, frame: Frame) -> None:
        """
        Goes in calibrate mode.
        """
        pass


class Capture(State):
    """
    In this state the user can scan the cube.
    """

    def run(self, frame: Frame) -> None:
        # Performs detections on the current frame, get the frame (edited by detector) and the color detection
        detection_frame, detection_colors = self.detection_handler.detect_colors(frame, self.palette)

        # Stores the current face
        self.face = Face(detection_colors.colors)

        # Draws the GUI
        frame_out = DrawerText.draw(detection_frame, [Command.EXIT.display_string], (0.025, 0.05))
        frame_out = DrawerText.draw(frame_out, [Command.CAPTURE.display_string], (0.40, 0.95))
        frame_out = DrawerCube.draw(frame_out, self.cube, self.palette)
        frame_out = DrawerFaceCapture.draw(frame_out, Face(detection_colors.colors), self.palette)
        self.gui = frame_out

    def help(self, frame: Frame) -> None:
        self.transition_to(Help())

    def reset(self) -> None:
        self.transition_to(Reset())

    def solve(self, frame: Frame) -> None:
        try:
            self.solution = Kociemba().solve(self.cube)
        except ValueError as e:
            self.solution = "Not a valid configuration"
            logging.error(f"Cannot solve the cube, error {str(e)}")
        self.transition_to(Solve())

    def capture(self) -> None:
        # Retrieves the side related to the central facet
        if self.face.center == Color.NONE:
            logging.info(f"Missing central tile color, cannot capture")
            return

        side = Cube.COLOR_TO_SIDE[self.face.center]

        # Updates the cube
        cube = self.cube
        cube.side_to_face[side] = self.face
        self.cube = cube

    def calibrate(self, frame: Frame) -> None:
        self.transition_to(CalibrateRed())


class Reset(State):
    """
    Resets the context and goes to Capture.
    """

    def help(self, frame: Frame) -> None:
        pass

    def reset(self) -> None:
        pass

    def solve(self, frame: Frame) -> None:
        pass

    def capture(self) -> None:
        pass

    def calibrate(self, frame: Frame) -> None:
        pass

    def run(self, frame: Frame) -> None:
        self.context.reset()
        self.transition_to(Capture())


class Help(State):
    """
    Shows the help on the GUI.
    """

    def help(self, frame: Frame) -> None:
        self.transition_to(Capture())

    def reset(self) -> None:
        pass

    def solve(self, frame: Frame) -> None:
        pass

    def capture(self) -> None:
        pass

    def calibrate(self, frame: Frame) -> None:
        pass

    def run(self, frame: Frame) -> None:
        offset_relative = (0.40, 0.35)
        self.gui = DrawerText.draw(frame, Command.help(), offset_relative)


class Calibrate(State, ABC):
    """
    Calibrates the colors.
    """

    @abc.abstractmethod
    def calibrate_color(self) -> Color:
        """
        Specifies the color of each specialized calibration state.

        :return: the color
        """
        pass

    def run(self, frame: Frame) -> None:
        # Detects the central facet
        detection_frame, dominant_bgr = self.detection_handler.detect_central_color(frame)
        self.color = dominant_bgr

        # Draws the GUI
        frame_out = DrawerText.draw(detection_frame, [Command.EXIT.display_string], (0.025, 0.05))
        frame_out = DrawerColorCapture.draw(frame_out, self.color)
        frame_out = DrawerPalette.draw(frame_out, self.palette)
        frame_out = DrawerText.draw(
            frame_out, [f"Scan the {self.calibrate_color().value.upper()} central tile"], (0.4, 0.95))
        self.gui = frame_out

    def help(self, frame: Frame) -> None:
        pass

    def reset(self) -> None:
        pass

    def solve(self, frame: Frame) -> None:
        pass

    def calibrate(self, frame: Frame) -> None:
        self.transition_to(Capture())


class CalibrateRed(Calibrate):
    """
    Calibrates the colors.
    """

    def calibrate_color(self) -> Color:
        return Color.RED

    def capture(self) -> None:
        if self.color:
            self.palette.red = self.color
        self.transition_to(CalibrateOrange())


class CalibrateOrange(Calibrate):
    """
    Calibrates the colors.
    """

    def calibrate_color(self) -> Color:
        return Color.ORANGE

    def capture(self) -> None:
        if self.color:
            self.palette.orange = self.color
        self.transition_to(CalibrateBlue())


class CalibrateBlue(Calibrate):
    """
    Calibrates the colors.
    """

    def calibrate_color(self) -> Color:
        return Color.BLUE

    def capture(self) -> None:
        if self.color:
            self.palette.blue = self.color
        self.transition_to(CalibrateGreen())


class CalibrateGreen(Calibrate):
    """
    Calibrates the colors.
    """

    def calibrate_color(self) -> Color:
        return Color.GREEN

    def capture(self) -> None:
        if self.color:
            self.palette.green = self.color
        self.transition_to(CalibrateWhite())


class CalibrateWhite(Calibrate):
    """
    Calibrates the colors.
    """

    def calibrate_color(self) -> Color:
        return Color.WHITE

    def capture(self) -> None:
        if self.color:
            self.palette.white = self.color
        self.transition_to(CalibrateYellow())


class CalibrateYellow(Calibrate):
    """
    Calibrates the colors.
    """

    def calibrate_color(self) -> Color:
        return Color.YELLOW

    def capture(self) -> None:
        if self.color:
            self.palette.yellow = self.color
        self.transition_to(Capture())


class Solve(State):
    """
    Solves the cube.
    """

    def run(self, frame: Frame) -> None:
        frame_out = DrawerText.draw(frame, [Command.EXIT.display_string], (0.025, 0.05))
        frame_out = DrawerText.draw(frame_out, [self.solution], (0.275, 0.500))
        frame_out = DrawerText.draw(frame_out, [f"Press SPACE to continue"], (0.40, 0.95))
        self.gui = frame_out

    def help(self, frame: Frame) -> None:
        pass

    def reset(self) -> None:
        pass

    def solve(self, frame: Frame) -> None:
        pass

    def capture(self) -> None:
        self.transition_to(Capture())

    def calibrate(self, frame: Frame) -> None:
        pass
