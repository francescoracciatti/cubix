"""
This module provides rendering facilities.
"""

from __future__ import annotations

import logging
import math
from typing import Tuple, List, Optional, Union

import cv2 as cv
import numpy as np

from src.video.palette import Palette, ColorBGR, ColorSpace


class Offset(object):
    """
    Models a generic offset.
    """

    def __init__(self, x: int, y: int) -> None:
        """
        Initializes the offset.

        :param x: the offset on the x axis
        :param y: the offset on the y axis
        """
        self.x: int = x
        self.y: int = y


class Point(object):
    """
    Models a generic point.
    """

    def __init__(self, x: int, y: int) -> None:
        """
        Initializes the point.

        :param x: the x coordinate
        :param y: the y coordinate
        """
        self.x: int = x
        self.y: int = y


class Rectangle(object):
    """
    Models a generic rectangle.
    """

    def __init__(self, top_left: (int, int), bottom_right: (int, int), color: ColorBGR, line_type: int = cv.FILLED
                 ) -> None:
        """
        Initializes the rectangle.

        :param top_left: the top-left corner
        :param bottom_right: the bottom-right corner
        :param color: the BGR color encoding
        :param line_type: the line type, cv.FILLED by default
        """
        self.top_left = top_left
        self.bottom_right = bottom_right
        self.color = color
        self.line_type = line_type


class Text(object):
    """
    Wraps generic single-line and multi-line text.
    """

    def __init__(self, text: List[str]) -> None:
        """
        Initializes the text.

        :param text: the text
        """
        self.text = text


# Type of OpenCV rect ((x_center, y_center), (width, height), angle)
CVRect = Tuple[Tuple[float, float], Tuple[float, float], float]


class RectBox(object):
    """
    Models a generic ROI as a rectangular box.
    """

    @classmethod
    def build_from_tf_bounding_box(cls, frame: Frame, bb: List[float]) -> RectBox:
        """
        Builds the rect box from the given TensorFlow BoundingBox.
        Note that TensorFlow BoundingBoxes are expressed in relative coordinates.

        :param frame: the source frame on which the BoundingBox was calculated
        :param bb: the TensorFlow BoundingBox [y_min, x_min, y_max, x_max]
        :return: the rect box
        """
        # Gets the absolute coordinates
        y_min = bb[0] * frame.height
        x_min = bb[1] * frame.width
        y_max = bb[2] * frame.height
        x_max = bb[3] * frame.width

        width = x_max - x_min
        height = y_max - y_min

        center_x = x_min + width / 2
        center_y = y_min + height / 2

        # TensorFlow BoundingBoxes are unrotated
        angle = 0.0

        return RectBox(((center_x, center_y), (width, height), angle))

    def __init__(self, cvrect: CVRect) -> None:
        """
        Initializes the rectangular box.

        :param cvrect: the OpenCV rect ((x_center, y_center), (width, height), angle)
        """
        self.cvrect = cvrect

    @property
    def width(self) -> int:
        return int(self.cvrect[1][0])

    @property
    def height(self) -> int:
        return int(self.cvrect[1][1])

    @property
    def area(self) -> int:
        return self.width * self.height

    @property
    def angle(self) -> float:
        """
        Gets the angle in degrees, in the range [0.0, 90.0), as defined in OpenCV.

        :return: the angle in degrees, in the range [0.0, 90.0)
        """
        return self.cvrect[2]

    @property
    def center(self) -> Point:
        return Point(int(self.cvrect[0][0]), int(self.cvrect[0][1]))

    @property
    def top_left(self) -> Point:
        return Point(self.center.x - self.width // 2,
                     self.center.y - self.height // 2)

    @property
    def top_right(self) -> Point:
        return Point(self.center.x + self.width // 2,
                     self.center.y - self.height // 2)

    @property
    def bottom_left(self) -> Point:
        return Point(self.center.x - self.width // 2,
                     self.center.y + self.height // 2)

    @property
    def bottom_right(self) -> Point:
        return Point(self.center.x + self.width // 2,
                     self.center.y + self.height // 2)

    @property
    def offset(self) -> Offset:
        return Offset(self.top_left.x, self.top_left.y)

    def resize(self, rate: float) -> RectBox:
        """
        Resizes the rectbox to the given rate.
        Note that the source rectbox remains unchanged.

        :rate: the resizing rate
        :return: the resized rectbox
        :raise: ValueError if rate is negative, or greater or equal to 1
        """
        if rate < 0 or rate >= 1:
            raise ValueError(f"Cannot resize rectbox of negative rate {rate}")

        width = self.width * rate
        height = self.height * rate

        return RectBox(((self.center.x, self.center.y), (width, height), self.angle))


# Type of OpenCV circle ((x_center, y_center), radius)
CVCircle = Tuple[Tuple[float, float], float]


class Circle(object):
    """
    Models a generic ROI as a circle.
    """

    def __init__(self, cvcircle: CVCircle) -> None:
        """
        Initializes the circle.

        :param cvcircle: the OpenCV circle ((x_center, y_center), radius)
        """
        self.cvcircle = cvcircle

    @property
    def center(self) -> Point:
        return Point(int(self.cvcircle[0][0]), int(self.cvcircle[0][1]))

    @property
    def radius(self) -> int:
        return int(self.cvcircle[1])

    @property
    def area(self) -> float:
        return math.pi * (self.radius ** 2)


# Type of OpenCV contour
CVContour = np.ndarray


class Frame(object):
    """
    Models a video frame and provides editing functions.
    """

    # Parameters for the rendering of objects
    DEFAULT_RADIUS = 2
    DEFAULT_THICKNESS = 3

    # Parameters for the rendering of text
    DEFAULT_FONT = cv.FONT_HERSHEY_SIMPLEX
    DEFAULT_FONT_SCALE = 0.75
    DEFAULT_FONT_THICK_INNER = 2
    DEFAULT_FONT_THICK_OUTER = 5

    def __init__(self, npframe: np.ndarray, color_space: ColorSpace, width: int, height: int) -> None:
        """
        Initializes the video frame.

        :param npframe: the video frame as a numpy multi-dimensional array
        :param color_space: the color space of the frame, e.g BGR, RGB, etc.
        :param height: the height of the frame
        :param width: the width of the frame
        """
        self.npframe: np.ndarray = npframe
        self.color_space: ColorSpace = color_space
        self.width: int = width
        self.height: int = height

    @property
    def area(self) -> int:
        """
        Gets the area of the frame.

        :return: the area of the frame
        """
        return self.width * self.height

    @property
    def center(self) -> Point:
        """
        Gets the center of the frame.

        :return: the center of the frame
        """
        return Point(self.width // 2, self.height // 2)

    def copy(self):
        """
        Gets a copy of the frame.
        """
        return Frame(self.npframe.copy(), self.color_space, self.width, self.height)

    def convert(self, target: ColorSpace) -> Frame:
        """
        Convert a copy of the source frame to the target color space.

        Supported conversion:
          * BGR <-> RGB
          * BGR <-> HSV
          * RGB <-> HSV
          * BGR <-> GRAY
          * RGB <-> GRAY

        :param target: the target color space
        :return: the frame in the target color space
        :raise: ValueError exception if the target conversion is not supported
        """
        if self.color_space == target:
            return self

        # BGR <-> RGB
        if self.color_space == ColorSpace.BGR and target == ColorSpace.RGB:
            conversion = cv.COLOR_BGR2RGB
        elif self.color_space == ColorSpace.RGB and target == ColorSpace.BGR:
            conversion = cv.COLOR_RGB2BGR
        # BGR <-> HSV
        elif self.color_space == ColorSpace.BGR and target == ColorSpace.HSV:
            conversion = cv.COLOR_BGR2HSV
        elif self.color_space == ColorSpace.HSV and target == ColorSpace.BGR:
            conversion = cv.COLOR_HSV2BGR
        # RGB <-> HSV
        elif self.color_space == ColorSpace.RGB and target == ColorSpace.HSV:
            conversion = cv.COLOR_RGB2HSV
        elif self.color_space == ColorSpace.HSV and target == ColorSpace.RGB:
            conversion = cv.COLOR_HSV2RGB
        # BGR <-> GRAY
        elif self.color_space == ColorSpace.BGR and target == ColorSpace.GRAY:
            conversion = cv.COLOR_BGR2GRAY
        elif self.color_space == ColorSpace.GRAY and target == ColorSpace.BGR:
            conversion = cv.COLOR_GRAY2BGR
        # RGB <-> GRAY
        elif self.color_space == ColorSpace.RGB and target == ColorSpace.GRAY:
            conversion = cv.COLOR_RGB2GRAY
        elif self.color_space == ColorSpace.GRAY and target == ColorSpace.RGB:
            conversion = cv.COLOR_GRAY2RGB

        else:
            err_msg = f'Conversion from {self.color_space.value} to {target.value} not supported'
            raise ValueError(err_msg)

        return Frame(cv.cvtColor(self.npframe.copy(), conversion), target, self.width, self.height)

    def draw(self,
             objs: List[Optional[Union[CVContour, Rectangle, RectBox, Circle, Point, Text]]],
             offset: Optional[Offset] = None,
             color: ColorBGR = Palette.CSS_BGR_GREEN  # FIXME color not used for Text
             ) -> Frame:
        """
        Draws the given list of object on a copy of the current frame.
        Note that the source frame remains unchanged.

        :param objs: the list of objects to draw
        :param offset: the optional offset, None by default
        :param color: the color, green by default
        :return: the frame with the rect box
        """
        frame_out = self.copy()

        if not objs:
            return frame_out

        if not offset:
            offset = Offset(0, 0)

        for obj in objs:
            # None, passes away
            if obj is None:
                pass

            # Draws the Contour
            elif type(obj) is CVContour:
                contour: CVContour = obj
                frame_out.npframe = cv.drawContours(
                    frame_out.npframe, [contour], 0, color, self.DEFAULT_THICKNESS, offset=(offset.x, offset.y))

            # Draws the Point
            elif type(obj) is Point:
                point: Point = obj
                center = (point.x + offset.x, point.y + offset.y)
                cv.circle(frame_out.npframe, center, self.DEFAULT_RADIUS, color, self.DEFAULT_THICKNESS)

            # Draws the Rectangle
            elif type(obj) is Rectangle:
                rect: Rectangle = obj
                if rect.line_type == cv.FILLED:
                    cv.rectangle(frame_out.npframe, rect.top_left, rect.bottom_right,
                                 rect.color, cv.FILLED)
                else:
                    cv.rectangle(frame_out.npframe, rect.top_left, rect.bottom_right,
                                 rect.color, self.DEFAULT_THICKNESS)

            # Draws the RectBox
            elif type(obj) is RectBox:
                rbox: RectBox = obj
                box = cv.boxPoints(rbox.cvrect)
                box = np.int0(box)
                frame_out.npframe = cv.drawContours(
                    frame_out.npframe, [box], -1, color, self.DEFAULT_THICKNESS, offset=(offset.x, offset.y))

            # Draws the Circle
            elif type(obj) is Circle:
                circle: Circle = obj
                center = (circle.center.x + offset.x, circle.center.y + offset.y)
                cv.circle(frame_out.npframe, center, circle.radius, color, self.DEFAULT_THICKNESS)

            # Draws the Text
            elif type(obj) is Text:
                text: Text = obj
                shift_y = 0
                for line in text.text:
                    (label_width, label_height), _ = cv.getTextSize(line,
                                                                    self.DEFAULT_FONT,
                                                                    self.DEFAULT_FONT_SCALE,
                                                                    self.DEFAULT_FONT_THICK_INNER)
                    if not offset:
                        x = int((frame_out.width - label_width) / 2)
                        y = int((frame_out.height - label_height) / 2 + shift_y)
                    else:
                        x = offset.x
                        y = offset.y + shift_y
                    shift_y += int(label_height * 1.75)  # Adds 75% of label' height between two lines

                    cv.putText(frame_out.npframe, line, (x, y),
                               self.DEFAULT_FONT, self.DEFAULT_FONT_SCALE,
                               Palette.CSS_BGR_BLACK, self.DEFAULT_FONT_THICK_OUTER)
                    cv.putText(frame_out.npframe, line, (x, y),
                               self.DEFAULT_FONT, self.DEFAULT_FONT_SCALE,
                               Palette.CSS_BGR_WHITE, self.DEFAULT_FONT_THICK_INNER)

            # Type not handled, cannot draw
            else:
                err_msg = f"Cannot draw {type(obj)}, type not handled"
                raise ValueError(err_msg)

        return frame_out

    # FIXME resulting frame may be wrongly cropped due to rotation
    def rotate(self, angle: float, center: Point) -> Frame:
        """
        Rotate the given frame around the center for the given angle, without cropping the frame edges.

        :param angle: test angle in degrees, positive values mean counter-clockwise test
        :param center: the center of rotation
        :return: the frame rotated
        """
        # The cv function getRotationMatrix2D calculates the following matrix:
        #  [ [ α, β, (1 − α) * center_x - β * center_y],
        #    [ -β, α, β * center_x + (1 − α) * center_y] ]
        # where:
        #  α = scale * cos(angle)
        #  β = scale * sin(angle)
        matrix = cv.getRotationMatrix2D((center.x, center.y), angle, scale=1.0)
        # cos = np.abs(matrix[0, 0])
        # sin = np.abs(matrix[0, 1])

        # Gets the new bounding dimensions of the image
        # width_adjusted = int((self.height * sin) + (self.width * cos))
        # height_adjusted = int((self.height * cos) + (self.width * sin))

        # Adjusts the test matrix to take into account translation
        # matrix[0, 2] += (width_adjusted / 2) - center.x
        # matrix[1, 2] += (height_adjusted / 2) - center.y

        # Rotates the frame
        rotated = cv.warpAffine(self.npframe, matrix, (self.width, self.height))
        return Frame(rotated, self.color_space, self.width, self.height)

    def crop(self, rbox: RectBox, align: bool = True) -> Frame:
        """
        Crops the frame enclosed in the given rect box.

        :param rbox: the rect box to crop
        :param align: if True aligns the frame horizontally before cropping, True by default
        :return: the cropped frame
        """
        # Rotates the source frame to align the rect box horizontally
        frame_to_crop = self.npframe
        if align and not np.isclose(rbox.angle, 0.0, rtol=1e-1):
            # Adjusts the angle if the rbox' angle exceeds 45 deg
            if rbox.angle > 45.0:
                angle = rbox.angle - 90.0  # Clockwise
            else:
                angle = rbox.angle  # Counter-clockwise
            logging.debug(f"RectBox angle {rbox.angle}, resulting rotation angle {angle}")
            frame_to_crop = self.rotate(angle, rbox.center).npframe

        x_min = rbox.center.x - rbox.width // 2
        x_max = rbox.center.x + rbox.width // 2
        y_min = rbox.center.y - rbox.height // 2
        y_max = rbox.center.y + rbox.height // 2

        return Frame(frame_to_crop[y_min:y_max, x_min:x_max], self.color_space, x_max - x_min, y_max - y_min)
