"""
This module provides the GUI and the video streaming handler.
"""

from abc import ABC
from typing import List, Tuple

from src.cube.cube import Side, Cube, Face, Color
from src.video.palette import Palette, ColorBGR
from src.video.render import Frame, Point, Text, Offset, Rectangle


class DrawerText(ABC):
    """
    Supports the drawing of text on screen.
    """

    @classmethod
    def draw(cls, frame_src: Frame, messages: List[str], offset: Tuple[float, float]) -> Frame:
        """
        Draws the calibration command on a copy of the given frame.
        Note that the source frame remains unchanged.

        :param frame_src: the source frame
        :param messages: the list of messages to draw on the frame
        :param offset: the (x, y) relative offset
        :return: the output frame with instructions
        """
        # Shows the text
        text = Text(messages)
        offset = Offset(int(frame_src.width * offset[0]), int(frame_src.height * offset[1]))
        frame_out = frame_src.draw([text], offset)  # draw method expects a list of objects

        return frame_out


class DrawerTile(ABC):
    """
    Supports the drawing of tiles.
    """

    TILE_SIDE_PIXEL = 25

    @classmethod
    def draw(cls, frame_src: Frame, color: ColorBGR, top_left: Point) -> Frame:
        """
        Draws the tile at the given position configuration of the given face at the given position (top-left corner).
        Note that the source frame remains unchanged.

        :param frame_src: the source frame
        :param color: the BGR color of the tile
        :param top_left: the top-left corner
        :return: the frame edited
        :raise: ValueError if wrong number of colors
        """
        pos_1 = (top_left.x, top_left.y)
        pos_2 = (top_left.x + DrawerTile.TILE_SIDE_PIXEL, top_left.y + DrawerTile.TILE_SIDE_PIXEL)
        rectangle = Rectangle(pos_1, pos_2, color)
        return frame_src.draw([rectangle])  # draw method expects a list of objects


class DrawerFace(ABC):
    """
    Supports the drawing of cube's faces on frames.
    """

    # Spacing between tiles in pixel
    TILE_SPACING_PIXEL = 2

    # Size of faces in pixel
    FACE_SIDE_PIXEL = 3 * DrawerTile.TILE_SIDE_PIXEL + 2 * TILE_SPACING_PIXEL

    @classmethod
    def draw(cls, frame_src: Frame, face: Face, palette: Palette, top_left: Point) -> Frame:
        """
        Draws the configuration of the given face at the given position (top-left corner).
        Note that the source frame remains unchanged.

        :param frame_src: the source frame
        :param face: the face to draw
        :param palette: the BGR color palette
        :param top_left: the top-left corner
        :return: the frame edited
        :raise: ValueError if wrong number of colors
        """
        # Prepares the list of rectangles to draw
        # by following the order: C1, C2, C3, C4, C5, C6, C7, C8, C9
        #
        #         |--------------|
        #         | C1 | C2 | C3 |
        #         |----|----|----|
        #         | C4 | C5 | C6 |
        #         |----|----|----|
        #         | C7 | C8 | C9 |
        #         |--------------|
        rectangles: List[Rectangle] = []
        for i in range(Face.TILES):
            x_min = int(top_left.x + (DrawerTile.TILE_SIDE_PIXEL + cls.TILE_SPACING_PIXEL) * (i % 3))
            y_min = int(top_left.y + (DrawerTile.TILE_SIDE_PIXEL + cls.TILE_SPACING_PIXEL) * int(i / 3))
            pos_1 = (x_min, y_min)
            pos_2 = (x_min + DrawerTile.TILE_SIDE_PIXEL, y_min + DrawerTile.TILE_SIDE_PIXEL)
            rectangles.append(Rectangle(pos_1, pos_2, palette.encoding(face.colors[i])))

        return frame_src.draw(rectangles)


class DrawerFaceCapture(ABC):
    """
    Supports the drawing of the face you are scanning.
    """

    # Relative offset (top-left corner)
    OFFSET_RELATIVE_X = 0.05
    OFFSET_RELATIVE_Y = 0.35

    @classmethod
    def draw(cls, frame_src: Frame, face: Face, palette: Palette) -> Frame:
        """
        Draws the configuration of the face you are currently scanning.
        Note that the source frame remains unchanged.

        :param frame_src: the source frame
        :param face: the face to draw
        :param palette: the BGR color palette
        :return: the frame edited
        :raise: ValueError if wrong number of colors
        """
        top_left = Point(int(cls.OFFSET_RELATIVE_X * frame_src.width + DrawerFace.FACE_SIDE_PIXEL),  # Adjusts X
                         int(cls.OFFSET_RELATIVE_Y * frame_src.height))
        return DrawerFace.draw(frame_src, face, palette, top_left)


class DrawerCube(ABC):
    """
    Supports the drawing of the configuration schema of the cube.
    """

    # Relative offset (top-left corner)
    OFFSET_RELATIVE_X = 0.05
    OFFSET_RELATIVE_Y = 0.55

    # Spacing between faces in pixel
    FACE_SPACING_PIXEL = 5

    # (x, y) relative coordinates of the top-left corner of each face
    SIDE_TO_POSITION = {
        Side.UP: (DrawerFace.FACE_SIDE_PIXEL, -FACE_SPACING_PIXEL),
        Side.LEFT: (-FACE_SPACING_PIXEL, DrawerFace.FACE_SIDE_PIXEL),
        Side.FRONT: (DrawerFace.FACE_SIDE_PIXEL, DrawerFace.FACE_SIDE_PIXEL),  # Center of the cross
        Side.RIGHT: (DrawerFace.FACE_SIDE_PIXEL * 2 + FACE_SPACING_PIXEL, DrawerFace.FACE_SIDE_PIXEL),
        Side.BACK: (DrawerFace.FACE_SIDE_PIXEL * 3 + FACE_SPACING_PIXEL * 2, DrawerFace.FACE_SIDE_PIXEL),
        Side.DOWN: (DrawerFace.FACE_SIDE_PIXEL, DrawerFace.FACE_SIDE_PIXEL * 2 + FACE_SPACING_PIXEL)
    }

    @classmethod
    def draw(cls, frame_src: Frame, cube: Cube, palette: Palette) -> Frame:
        """
        Draws the configuration scheme of the cube, at the given position (top-left corner).
        Note that the source frame remains unchanged.

        :param frame_src: the source frame
        :param cube: the cube
        :param palette: the BGR color palette
        :return: the frame edited
        :raise: ValueError if wrong number of colors
        """
        # Draws each face of the cube
        frame_out = frame_src
        for side, pos in cls.SIDE_TO_POSITION.items():
            # Gets the absolute coordinates (top-left corner) of the current face
            coord_min = Point(pos[0] + int(cls.OFFSET_RELATIVE_X * frame_src.width),
                              pos[1] + int(cls.OFFSET_RELATIVE_Y * frame_src.height))

            # Draws the current face
            frame_out = DrawerFace.draw(frame_out, cube.face(side), palette, coord_min)

        return frame_out


class DrawerColorCapture(ABC):
    """
    Supports the drawing of the face you are scanning.
    """

    # Relative offset (top-left corner)
    OFFSET_RELATIVE_X = 0.05
    OFFSET_RELATIVE_Y = 0.40

    @classmethod
    def draw(cls, frame_src: Frame, color: ColorBGR) -> Frame:
        """
        Draws the configuration of the face you are currently scanning.
        Note that the source frame remains unchanged.

        :param frame_src: the source frame
        :param color: the BGR color
        :return: the frame edited
        :raise: ValueError if wrong number of colors
        """
        top_left = Point(int(cls.OFFSET_RELATIVE_X * frame_src.width), int(cls.OFFSET_RELATIVE_Y * frame_src.height))
        return DrawerTile.draw(frame_src, color, top_left)


class DrawerPalette(ABC):
    """
    Supports the drawing of the current palette.
    """

    # Relative offset palette (top-left corner)
    OFFSET_PALETTE_RELATIVE_X = 0.05
    OFFSET_PALETTE_RELATIVE_Y = 0.55

    # Relative text palette (top-left corner)
    OFFSET_TEXT_RELATIVE_X = 0.050
    OFFSET_TEXT_RELATIVE_Y = 0.525

    # Spacing between tiles in pixel
    TILE_SPACING_PIXEL = 2  # Spacing among tiles

    # Spacing between text and tiles
    TEXT_SPACING_PIXEL = 5
    TEXT_SHIFT_Y_PIXEL = 20

    @classmethod
    def draw(cls, frame_src: Frame, palette: Palette) -> Frame:
        """
        Draws the current palette on the given frame.
        Note that the source frame remains unchanged.

        :param frame_src: the source frame
        :param palette: the color palette
        :return: the frame edited
        :raise: ValueError if wrong number of colors
        """
        text = Text(["Palette"])
        offset = Offset(int(frame_src.width * cls.OFFSET_TEXT_RELATIVE_X),
                        int(frame_src.height * cls.OFFSET_TEXT_RELATIVE_Y))
        frame_out = frame_src.draw([text], offset)  # draw method expects a list of objects

        # Loops over colors
        for i, color in enumerate(Color.list()):  # The ordering follows that of cube.Colors

            coord_x = int(cls.OFFSET_PALETTE_RELATIVE_X * frame_out.width)
            coord_y = int(cls.OFFSET_PALETTE_RELATIVE_Y * frame_out.height
                          + i * (DrawerTile.TILE_SIDE_PIXEL + cls.TILE_SPACING_PIXEL))

            top_left = Point(coord_x, coord_y)
            frame_out = DrawerTile.draw(frame_out, palette.encoding(color), top_left)

            # Draw the color name
            offset_text = Offset(coord_x + DrawerTile.TILE_SIDE_PIXEL + cls.TEXT_SPACING_PIXEL,
                                 coord_y + cls.TEXT_SHIFT_Y_PIXEL)
            frame_out = frame_out.draw([Text([f"{color.value.upper()}"])], offset_text)
        return frame_out
