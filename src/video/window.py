"""
This module provides the handling mechanism for windows.
"""

from __future__ import annotations

import configparser
import logging
from abc import ABC
from enum import unique, Enum
from typing import Tuple, Optional, Dict

import cv2 as cv

from src.video.render import Frame, Point


@unique
class WindowLayer(Enum):
    """
    Layers of windows.
    """
    MAIN = 0
    DEBUG = 1


class Window(object):
    """
    Convenience class to wrap frames in windows.
    """

    def __init__(self, frame: Frame, name: str, layer: WindowLayer) -> None:
        """
        Initializes the window with the given frame, name and layer.

        :param frame: the frame
        :param name: the name of the window
        :param layer: the layer of the window
        """
        self.frame = frame
        self.name = name
        self.layer = layer
        # Default position
        self.top_left: Point = Point(0, 0)
        # Default size
        self.size: Tuple[int, int] = (self.frame.width, self.frame.height)

    def __str__(self) -> str:
        return f"{self.__class__.__name__} {{name {self.name}, layer {self.layer}}}"

    def move(self, top_left: Point) -> Window:
        """
        Moves this window in the given position.

        :param top_left: the top-left position
        :return: the window
        """
        self.top_left = top_left
        return self

    def resize(self, size: Tuple[int, int]) -> Window:
        """
        Resizes this window.

        :param size: the tuple containing the (width, height)
        :return: the window
        """
        self.size = size
        return self

    def show(self) -> None:
        """
        Shows this window.
        """
        # Resizes the frame if needed
        if self.frame.width != self.size[0] or self.frame.height != self.size[1]:
            cv.imshow(self.name, cv.resize(self.frame.npframe, self.size))
        else:
            cv.imshow(self.name, self.frame.npframe)

        # Moves the frame if needed
        if self.top_left.x != 0 or self.top_left != 0:
            cv.moveWindow(self.name, self.top_left.x, self.top_left.y)


class WindowHandler(ABC):
    """
    Handles the windows to show on screen.

    Windows are displayed in order, by increasing layer.
    This means that higher layers are stacked up to lower layers.
    """

    # Shows windows having the same or lower layer
    SHOW_UP_TO_LAYER: Optional[WindowLayer] = WindowLayer.MAIN

    # The list of registered windows
    NAME_TO_WINDOW: Dict[str, Window] = {}

    @classmethod
    def configure(cls, path_to_configuration_file: str) -> None:
        """
        Configures the window handler.
        """
        # Reads the configuration file
        configuration = configparser.ConfigParser()
        configuration.read(path_to_configuration_file)

        # Shows/hides image processing
        show_image_processing = configuration['ImageProcessing']['Show']
        if show_image_processing.lower() == 'true':
            logging.info(f"WindowHandler will show ImageProcessing windows")
            cls.SHOW_UP_TO_LAYER = WindowLayer.DEBUG

    @classmethod
    def register(cls, window: Window) -> None:
        """
        Registers the given window by name.
        Is the name already exists, it is overwritten.

        :param window: the window to be registered
        """
        cls.NAME_TO_WINDOW[window.name] = window

    @classmethod
    def unregister(cls, name: str) -> None:
        """
        Unregisters and destroys the window with the given name.

        :param name: the name of the window to unregister
        """
        if name in cls.NAME_TO_WINDOW.keys():
            cv.destroyWindow(name)
            del cls.NAME_TO_WINDOW[name]

    @classmethod
    def close(cls) -> None:
        """
        Closes all the windows in decreasing layer ordering.
        """
        windows = list(cls.NAME_TO_WINDOW.values())
        windows.sort(key=lambda w: w.layer.value, reverse=True)  # Sorts by decreasing layer values

        for window in windows:
            cls.unregister(window.name)

    @classmethod
    def show(cls) -> None:
        """
        Shows the windows, in increasing layer ordering.
        """
        windows = list(cls.NAME_TO_WINDOW.values())
        windows.sort(key=lambda w: w.layer.value, reverse=False)  # Sorts by increasing layer values

        for window in windows:
            if window.layer.value <= cls.SHOW_UP_TO_LAYER.value:
                window.show()
