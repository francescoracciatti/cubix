"""
This module provides a customizable color palette.
"""

from enum import unique, Enum
from typing import Tuple, Union

from src.cube.cube import Color

# Type of BGR Color
ColorBGR = Tuple[int, int, int]


@unique
class ColorSpace(Enum):
    """
    Supported color spaces.
    """
    BGR = 'BGR'
    RGB = 'RGB'
    HSV = 'HSV'
    GRAY = 'GRAY'


class Palette(object):
    """
    Provides a generic color palette.
    """

    # Default CSS colors in BGR notation
    CSS_BGR_RED: ColorBGR = (0, 0, 255)
    CSS_BGR_GREEN: ColorBGR = (0, 255, 0)
    CSS_BGR_ORANGE: ColorBGR = (0, 165, 255)
    CSS_BGR_BLUE: ColorBGR = (255, 0, 0)
    CSS_BGR_WHITE: ColorBGR = (255, 255, 255)
    CSS_BGR_YELLOW: ColorBGR = (0, 255, 255)
    CSS_BGR_GRAY: ColorBGR = (125, 125, 125)
    CSS_BGR_BLACK: ColorBGR = (0, 0, 0)

    def __init__(self, color_space: ColorSpace) -> None:
        """
        Initializes the palette.

        :param color_space: the color space of the palette
        :raise: ValueError if the color space is not recognized
        """
        # The color space
        self.color_space = color_space

        # The colors
        if color_space == ColorSpace.BGR:
            self.red = self.CSS_BGR_RED
            self.green = self.CSS_BGR_GREEN
            self.orange = self.CSS_BGR_ORANGE
            self.blue = self.CSS_BGR_BLUE
            self.white = self.CSS_BGR_WHITE
            self.yellow = self.CSS_BGR_YELLOW
        else:
            raise ValueError(f"ColorSpace {color_space} not handled")

    def encoding(self, color: Color) -> Union[ColorBGR]:
        """
        Gets the encoding of the given color.

        :param color: the color
        :return: the encoding of the given color
        :raise: ValueError if the color is not recognized
        """
        if color == color.RED:
            return self.red
        if color == color.ORANGE:
            return self.orange
        if color == color.BLUE:
            return self.blue
        if color == color.GREEN:
            return self.green
        if color == color.WHITE:
            return self.white
        if color == color.YELLOW:
            return self.yellow
        if color == color.NONE:  # Maps Color.NONE on GRAY
            return self.CSS_BGR_GRAY
        raise ValueError(f"{color} color not recognized")
