"""
This module provides the GUI and the video streaming handler.
"""

import logging
import sys

import cv2 as cv

from src.detection.cube import CubeDetector
from src.state.command import Keyboard, Command
from src.state.context import Context, Capture
from src.video.render import Frame, ColorSpace
from src.video.window import Window, WindowHandler, WindowLayer


class Executor(object):
    """
    Executes commands.
    """

    @classmethod
    def execute(cls, command: Command, context: Context, frame: Frame) -> None:
        """
        Executes the given command, on the current context.

        :param command: the input command
        :param context: the current context
        :param frame: the frame with which update the GUI, depending on the context
        """
        if command == Command.NONE:
            context.request_run(frame)
        elif command == Command.HELP:
            context.request_help(frame)
        elif command == Command.RESET:
            context.request_reset()
        elif command == Command.SOLVE:
            context.request_solve(frame)
        elif command == Command.CAPTURE:
            context.request_capture()
        elif command == Command.CALIBRATE:
            context.request_calibrate(frame)
        else:
            raise ValueError(f"Cannot execute {command}, request not implemented yet")


class Video(object):
    """
    Streams the detection results on the video.
    """

    @staticmethod
    def configure(path_to_configuration_file: str) -> None:
        """
        Configures the video streamer.
        """
        WindowHandler.configure(path_to_configuration_file)

    def __init__(self, video_index: int, cube_detector: CubeDetector) -> None:
        """
        Initializes the video streamer.

        :param video_index: the index of the video capturing device
        :param cube_detector: the cube detector
        """
        self.video_index = video_index
        self.context = Context(Capture(), cube_detector)

    def run(self) -> None:
        """
        Streams the video.
        """
        # Gets the frame from the camera device
        video = cv.VideoCapture(self.video_index)
        width = int(video.get(cv.CAP_PROP_FRAME_WIDTH))
        height = int(video.get(cv.CAP_PROP_FRAME_HEIGHT))

        # Exits if cannot open video
        if not video.isOpened():
            logging.critical("Cannot open video")
            sys.exit()

        run = True
        while run:
            # Captures the current video frame
            run, video_frame = video.read()
            if run:
                # Gets the current frame, flips the frame for user convenience
                frame_raw = cv.flip(video_frame, 1)
                frame = Frame(frame_raw, ColorSpace.BGR, width, height)

                # Catches the input command, if any, and updates the context
                command = Keyboard.catch()

                if command != Command.EXIT:
                    Executor.execute(command, self.context, frame)
                    # Gets the GUI from the context and draws it on the main window
                    window = Window(self.context.gui.convert(ColorSpace.BGR), "Cubix", WindowLayer.MAIN)
                    WindowHandler.register(window)
                    WindowHandler.show()
                else:
                    run = False
            else:
                logging.critical("Cannot read video")

        WindowHandler.close()
        sys.exit()
