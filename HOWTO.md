# HowTo
* [Install TensorFlow Object Detection API](#install-tensorflow-object-detection-api)
* [Train your custom TensorFlow detector](#train-your-custom-tensorflow-detector)
* [Change tracing level](#change-tracing-level)
* [Show image processing details](#show-image-processing-details)
* [Use a custom TensorFlow-based detector](#use-a-custom-tensorflow-based-detector)
* [Use a custom non-TensorFlow-based detector](#use-a-custom-non-tensorflow-based-detector)
* [Use a custom facets detector](#use-a-custom-facets-detector)

## Install TensorFlow Object Detection API
Please, refer to the [TensorFlow official docs]
(https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html).

## Train your custom TensorFlow detector
Training a custom detector may be an expensive task, in terms of time and computational power.\
There are several ways to train your network. [Google Colab](https://colab.research.google.com/) is one of them.\
It is cloud-based, it is fast, and it is free.

## Change tracing level
To change the tracing leve, edit the `configuration.ini` file. 
For example, to set the `Info` level:
```ini
# Trace levels
#  - Critical
#  - Error
#  - Warning
#  - Info
#  - Debug
[Trace]
Level = Info
```

## Show image processing details
To show details regarding the image processing, edit the `configuration.ini` file. 
```ini
# Show image processing (useful to develop and debug)  
#  - True
#  - False
[ImageProcessing]
Show = True
```
It will show a set of debug windows to provide useful insights regarding image processing.  
It may be useful for developing or debugging.

## Use a custom TensorFlow-based detector
Cubix comes bundled with a default pre-trained cube detector based on TensorFlow, 
to detect the cube in the video stream.
 
You can use your custom TensorFlow based detector by editing the file `configuration.ini`:
```ini
[Detection]
Platform = TensorFlow

[Detection.TensorFlow]
PathToSavedModel = <PATH_TO_CUSTOM_SAVED_MODEL>
PathToLabelMap = <PATH_TO_CUSTOM_LABEL_MAP>
```
You have to provide the paths to the TensorFlow saved model and its LabelMap, only.


## Use a custom non-TensorFlow-based detector
Though Cubix comes with a pre-trained TensorFlow-based detector, it is *platform-agnostic*.\
So, you can use a detector based on any platform, other than TensorFlow.\
But you have to get your hands a little dirty :).

In general, you have to provide new entries in the configuration file
and write down some code to let Cubix using the desired detector.\
Don't worry, it is a simple task since Cubix was designed for that.

As a first step, 
in the configuration file `configuration.ini` you have to write down the entries that your platform needs.
Consider that the desired platform may use a configuration mechanism different from that of TensorFlow.
So, it may be necessary to change and/or extend the default entries, for example:
```ini
[Detection]
Platform = MyCustomPlatform  # Will use MyCustomPlatform instead of TensorFlow

[Detection.TensorFlow]
PathToSavedModel = <PATH_TO_CUSTOM_SAVED_MODEL>
PathToLabelMap = <PATH_TO_CUSTOM_LABEL_MAP>

# Add your custom configuration, will use the entries below
[Detection.MyCustomPlatform]
PathToMyPlatformStuff_1 = <PATH_TO_CUSTOM_STUFF_1>
PathToMyPlatformStuff_2 = <PATH_TO_CUSTOM_STUFF_2>
...
PathToMyPlatformStuff_N = <PATH_TO_CUSTOM_STUFF_N>
```
From here on, all the magic is in the file `src/detection/detection.py`. 

As a second step, you have to handle the new entries of `[Detection.MyCustomPlatform]` 
in the class `CubeDetectorConfigurator` in order to let Cubix to instantiate the new detector.

As a third and final step (the more complex), you have to implement the new detector 
by leveraging on the Abstract Factory Pattern.\
In particular, you have to provide a new *concrete detector creator* for the base class `CubeDetectorCreator`.\
Such a concrete detector creator will be triggered at runtime to instantiate a *concrete cube detector*.\
The concrete cube detector must inherit from the base class `CubeDetector`, and implement its abstract methods.\
Keep in mind that the result of the detection process must be wrapped in the class `Detection`.

That's all.

You can use the class `TensorFlowCubeDetector` as an example of how things work.

## Use a custom facets detector
The default facet detector is based on OpenCV. It is located in `detection/facet.py`.
You may customize it, or, alternatively, you may build a new one.
The `FacetDetector` is used by the `DetectionHandler`, which is located in `detection/handler.py`.
In particular, the `DetectionHandler` uses the `FaceDetector` to:
* detect the colors of the cube's face, while scanning the cube, 
  through the method `DetectionHandler.detect_colors()`;
* detect the color of the central tile, while calibrating colors, 
  through the method `DetectionHandler.detect_central_color()`.
  