# TODO
In descending priority order.

Highest

- Fix face 90deg inversion due to cw/ccw rotation on small angles. The face should rotate for angles >= 45 deg.
- Refactor detection package, simplify `detection.ObjectDetection`. 

Lowest